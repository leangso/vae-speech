import numpy as np
import feat.util as util
import feat.mfcc as mfcc
import feat.sscf as sscf
import feat.filter as filter
import feat.gfcc as gfcc
import feat.preprocess as pre
import librosa


SSCFS_IDX  = [0, 1, 2, 3, 4, 5]
SSCFS_PLANE_IDX = [0, 1, 2, 3, 4]    # SSCF planes, 0: SSCF0/SSCF1
SSCMS_IDX  = [0, 1, 2, 3, 4, 5]

def extract_features(
        signal,
        sample_rate=16000,
        pre_emph=0.97,
        win_len=0.025,
        win_step=0.01,
        win_type='hamming',
        fft_size=512,
        fmin=0,

        mfcc_enable=False,
        mfcc_filter=None,
        mfcc_num_cep=6,
        mfcc_num_filt=6,
        mfcc_normalize=None,
        mfcc_append_delta=False,

        fbank_enable=False,
        fbank_filter=None,
        fbank_power=2,
        fbank_norm_power=True,
        fbank_num_filt=40,
        fbank_normalize=None,          # cmn | cvn
        fbank_append_delta=False,

        sscm_enable=False,
        sscm_exclude='',
        sscm_filter=None,
        sscm_num_filt=40,
        sscm_normalize=None,
        sscm_append_delta=False,

        sscf_enable=False,
        sscf_filter=None,
        sscf_num_filt=40,
        sscf_include='',
        sscf_smooth_step=1,
        sscf_normalize_sscf0=None,
        sscf_normalize=None,
        sscf_normalize_exclude_sscfs='',
        sscf_append_delta=False,
        
        gfcc_enable=False,
        gfcc_filter=None,
        gfcc_num_cep=6,
        gfcc_num_filt=6,
        gfcc_normalize=None,
        gfcc_append_delta=False,

        spec_enable=False,
        **kwargs):
 
    feats = []
    Ds = []
    DDs = []

    ###### MFCC ######
    if mfcc_enable:
        filt_banks = _get_filter_banks(mfcc_filter)
        mfccs = mfcc.mfccs(
            sig=signal, 
            num_cep=mfcc_num_cep, 
            num_filt=mfcc_num_filt,
            num_fft=fft_size,
            sr=sample_rate, 
            low_freq=fmin,
            win_len=win_len, 
            win_step=win_step, 
            fbanks=filt_banks)

        if mfcc_normalize:
            if mfcc_normalize == 'cmn':
                mfccs = util.cmn(mfccs)
            elif mfcc_normalize == 'cvn':
                mfccs = util.cvn(mfccs)
            else:
                raise Exception('Unsupport normalization method: ', mfcc_normalize)

        feats.append(mfccs)
        
        if mfcc_append_delta:
            D, DD = _compute_deltas(mfccs)
            Ds.append(D)
            DDs.append(DD)

    ###### Fbank ######
    if fbank_enable:
        filt_banks = _get_filter_banks(fbank_filter)
        fbanks, _ = mfcc.mel_spectrogram(
            sig=signal, 
            num_filt=fbank_num_filt,
            num_fft=fft_size,
            sr=sample_rate, 
            low_freq=fmin,
            win_len=win_len, 
            win_step=win_step,
            power=fbank_power,
            norm_power=fbank_norm_power, 
            fbanks=filt_banks)
        
        if fbank_normalize:
            if fbank_normalize == 'cmn':
                fbanks = util.cmn(fbanks)
            elif fbank_normalize == 'cvn':
                fbanks = util.cvn(fbanks)
            elif fbank_normalize == 'log':
                fbanks = util.handle_zero(fbanks)
                fbanks = np.log(fbanks)
            elif fbank_normalize.startswith('db_'):
                top_db = int(fbank_normalize.split('_')[1])
                fbanks = librosa.amplitude_to_db(fbanks.T, top_db=top_db).T
                fbanks = fbanks / top_db + 1
            else:
                raise Exception('Unsupport normalization method: ', fbank_normalize)

        feats.append(fbanks)
        
        if fbank_append_delta:
            D, DD = _compute_deltas(fbanks)
            Ds.append(D)
            DDs.append(DD)

    ###### SSCM ######
    if sscm_enable:
        filt_banks = _get_filter_banks(sscm_filter)
        sscms = sscf.sscms(
            signal, 
            sr=sample_rate, 
            win_len=win_len, 
            win_step=win_step, 
            num_fft=fft_size, 
            low_freq=fmin,
            num_filt=sscm_num_filt,
            fbanks=filt_banks)

        if sscm_normalize:
            if sscm_normalize == 'cmn':
                sscms = util.cmn(sscms)
            elif sscm_normalize == 'cvn':
                sscms = util.cvn(sscms)
            else:
                raise Exception('Unsupport normalization method: ', sscm_normalize)

        if sscm_exclude:
            include_sscms = list(set(SSCMS_IDX) - set(eval(sscm_exclude)))
            sscms = sscms[:, include_sscms]
        
        feats.append(sscms)

        if sscm_append_delta:
            D, DD = _compute_deltas(sscms)
            Ds.append(D)
            DDs.append(DD)

    ###### SSCF ######
    if sscf_enable:
        filt_banks = _get_filter_banks(sscf_filter)
        sscfs = sscf.sscfs(
            signal, 
            sr=sample_rate, 
            win_len=win_len, 
            win_step=win_step, 
            num_fft=fft_size, 
            low_freq=fmin,
            num_filt=sscf_num_filt,
            fbanks=filt_banks)

        if sscf_smooth_step > 1:
            sscfs = util.average(sscfs, win_len=sscf_smooth_step)

        if sscf_normalize_sscf0:
            if sscf_normalize_sscf0 == 'st100':
                sscfs[:, 0] = util.semitone(sscfs[:, 0], 100)
            elif sscf_normalize_sscf0 == 'stavg':
                sscfs[:, 0] = util.semitone_avg(sscfs[:, 0])
            elif sscf_normalize_sscf0 == 'cmn':
                sscfs[:, 0] = util.cmn(sscfs[:, 0])
            elif sscf_normalize_sscf0 == 'cvn':
                sscfs[:, 0] = util.cvn(sscfs[:, 0])
            else:
                raise Exception('Unsupport normalization method: ', sscf_normalize_sscf0)

        if sscf_normalize:
            if sscf_normalize == 'ratio3':
                include_sscfs = list(set(SSCFS_IDX) - set(eval(sscf_normalize_exclude_sscfs)))
                for idx in include_sscfs:
                    sscfs[:, idx] = sscfs[:, idx] / sscfs[:, 3]
            elif sscf_normalize == 'cmn':
                include_sscfs = list(set(SSCFS_IDX) - set(eval(sscf_normalize_exclude_sscfs)))
                for idx in include_sscfs:
                    sscfs[:, idx] = util.cmn(sscfs[:, idx])
            elif sscf_normalize == 'cvn':
                include_sscfs = list(set(SSCFS_IDX) - set(eval(sscf_normalize_exclude_sscfs)))
                for idx in include_sscfs:
                    sscfs[:, idx] = util.cvn(sscfs[:, idx])
            elif sscf_normalize == 'savgol_filter':
                include_sscfs = list(set(SSCFS_IDX) - set(eval(sscf_normalize_exclude_sscfs)))
                for idx in include_sscfs:
                    sscfs[:, idx] = util.savgol_filter(sscfs[:, idx], window_length=5, polyorder=2)
            else:
                raise Exception('Unsupport normalize method: ', sscf_normalize) 

        if sscf_include:
            include_sscfs = eval(sscf_include)
            feats.append(sscfs[:, include_sscfs])

            if sscf_append_delta:
                D, DD = _compute_deltas(sscfs[:, include_sscfs])
                Ds.append(D)
                DDs.append(DD)

    ###### GFCC ######
    if gfcc_enable:
        filt_banks = _get_filter_banks(gfcc_filter)
        gfccs = gfcc.gfccs(
            sig=signal, 
            num_cep=gfcc_num_cep, 
            num_filt=gfcc_num_filt,
            num_fft=fft_size,
            low_freq=fmin,
            sr=sample_rate, 
            win_len=win_len, 
            win_step=win_step,
            fbanks=filt_banks)

        if gfcc_normalize:
            if gfcc_normalize == 'cmn':
                gfccs = util.cmn(gfccs)
            elif gfcc_normalize == 'cvn':
                gfccs = util.cvn(gfccs)
            else:
                raise Exception('Unsupport normalization method: ', mfcc_normalize)

        feats.append(gfccs)
        
        if gfcc_append_delta:
            D, DD = _compute_deltas(gfccs)
            Ds.append(D)
            DDs.append(DD)

    ###### Raw Spectrogram ######
    if spec_enable:
        frames, _ = pre.preprocess(sig=signal, sr=sample_rate, pre_emph=pre_emph, win_len=win_len, win_step=win_step, win_type=win_type)
        specs = pre.fft(frames, fft_size)

        feats.append(specs)
    
    ############        
    if len(feats) == 0:
        combine_feats = []
    else:
        combine_feats = np.hstack(feats)
        if len(Ds) > 0:
            combine_feats = np.column_stack([combine_feats, np.hstack(Ds), np.hstack(DDs)]) 

    return combine_feats


def _compute_deltas(feat, N=2):
    D = util.delta(feat, N)
    DD = util.delta(D, N)
    return D, DD


def _get_filter_banks(name):
    if name is None or name == '':
        return None
    elif name == 'phuong':
        return filter.get_fbank_phuong()
    elif name == '8mfcc':
        return filter.get_fbank_8mfcc()
    elif name == 'new_fbank_v2':
        return filter.get_new_fbank_v2()
    elif name == 'new_fbank_v3':
        return filter.get_new_fbank_v3()
    elif name == 'new_fbank_v4':
        return filter.get_new_fbank_v4()
    else:
        raise Exception('Unsupport filter: ', name)
