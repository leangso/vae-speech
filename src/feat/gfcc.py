import numpy as np

from typing import Tuple, Union
from spafe.fbanks.gammatone_fbanks import generate_center_frequencies, compute_gain
from feat.preprocess import preprocess, fft_magnitude_spec
from feat.util import handle_zero, dct, liftering, magnitude2power, hz2erb


# Slaney's ERB Filter constants
EarQ = 9.26449
minBW = 24.7


def gammatone_fbanks(
    num_filt: int = 40,
    num_fft: int = 512,
    sr: int = 16000,
    low_freq: float = 0,
    high_freq: float = None,
    order: Union[int, np.ndarray] = 4,
    factor=1,
    cnt_freqs: np.ndarray = None,
) -> Tuple[np.ndarray, np.ndarray]:
    """
        Returns:
            tuple:
                - numpy.ndarray : array of size num_filts * (num_fft/2 + 1) containing filter bank. Each row holds 1 filter.
                - numpy.ndarray : array of center frequencies in Erb.
    """ 

    # init freqs
    high_freq = high_freq or sr / 2
    
    assert low_freq >= 0, 'low_freq cannot < 0'
    assert high_freq <= (sr / 2), 'high_freq cannot > fs'
    
    # define custom difference func
    def Dif(u, a):
        return u - a.reshape(num_filt, 1)
    
    # computer center frequencies, convert to ERB scale and compute bandwidths
    if cnt_freqs is None:
        fcs = generate_center_frequencies(low_freq, high_freq, num_filt)
    else:
        fcs = cnt_freqs
        num_filt = len(cnt_freqs)

    # init vars
    fbank = np.zeros([num_filt, num_fft])
    maxlen = num_fft // 2 + 1
    T = 1 / sr
    n = 4
    u = np.exp(1j * 2 * np.pi * np.array(range(num_fft // 2 + 1)) / num_fft)
    idx = range(num_fft // 2 + 1)

    ERB = factor * ((fcs / EarQ) ** order + minBW**order) ** (1 / order)
    B = 1.019 * 2 * np.pi * ERB

    # compute input vars
    wT = 2 * fcs * np.pi * T
    pole = np.exp(1j * wT) / np.exp(B * T)

    # compute gain and A matrix
    A, Gain = compute_gain(fcs, B, wT, T)

    # compute fbank
    fbank[:, idx] = (
        (T**4 / Gain.reshape(num_filt, 1))
        * np.abs(Dif(u, A[0]) * Dif(u, A[1]) * Dif(u, A[2]) * Dif(u, A[3]))
        * np.abs(Dif(u, pole) * Dif(u, pole.conj())) ** (-n)
    )

    # make sure all filters has max value = 1.0
    try:
        fbank = np.array([f / np.max(f) for f in fbank[:, range(maxlen)]])
    except BaseException:
        fbank = fbank[:, idx]

    return fbank, np.array([hz2erb(freq) for freq in fcs])


def erb_spectrogram(
    sig: np.ndarray,
    num_filt: int = 40,
    num_fft: int = 512,
    sr: int = 16000,
    pre_emph: float = 0.97,
    win_len: float = 0.025, 
    win_step: float = 0.01,
    win_type: str = "hamming",
    low_freq: float = 0,
    high_freq: float = None,
    fbanks: np.ndarray = None
) -> Tuple[np.ndarray, np.ndarray]:
    """
        Returns:
            tuple:
                - numpy.ndarray: the erb spectrogram (num_frames x num_filts)
                - numpy.ndarray: the FFT in power spec.
    """

    # get fbanks
    if fbanks is None:
        gamma_fbanks, _ = gammatone_fbanks(
            num_filt=num_filt,
            num_fft=num_fft,
            sr=sr,
            low_freq=low_freq,
            high_freq=high_freq,
        )
        fbanks = gamma_fbanks

    # preprocess
    frames, _ = preprocess(sig, sr, pre_emph, win_len, win_step, win_type)

    # FFT -> |FFT|
    mag_spec = fft_magnitude_spec(frames, num_fft)

    # power spectrum
    pow_spec = magnitude2power(mag_spec, num_fft)

    # gammatone spectrogram
    gammatone_spec = np.dot(pow_spec, fbanks.T)

    return gammatone_spec, pow_spec


def gfccs(
    sig: np.ndarray,
    num_cep: int = 13,
    num_filt: int = 40,
    num_fft: int = 512,
    sr: int = 16000,
    pre_emph: float = 0.97,
    win_len: float = 0.025, 
    win_step: float = 0.01,
    win_type: str = "hamming",
    low_freq: float = 0,
    high_freq: float = None,
    non_linear_tf: str = 'log',
    use_energy: bool = False,
    lifter: int = 22,
    fbanks: np.ndarray = None,
) -> np.ndarray:
    """
        Returns:
            numpy.ndarray: GFCCs (num_frames x num_ceps)
    """

    # run checks
    if num_filt < num_cep:
        raise Exception("num_filts must be >= num_ceps")

    # compute erb spectrogram
    gammatone_spec, pow_spec = erb_spectrogram(
        sig=sig, 
        num_filt=num_filt, 
        num_fft=num_fft, 
        sr=sr, 
        pre_emph=pre_emph, 
        win_len=win_len, 
        win_step=win_step, 
        win_type=win_type, 
        low_freq=low_freq, 
        high_freq=high_freq, 
        fbanks=fbanks
    )

    # non-linear transform
    if non_linear_tf == 'log':
        gammatone_spec = handle_zero(gammatone_spec)
        gammatone_spec = np.log(gammatone_spec)
    else:
        gammatone_spec = np.power(gammatone_spec, 1 / 3)

    # DCT
    gfccs = dct(gammatone_spec)[:, :num_cep]

    # use energy for 1st features column
    if use_energy:
        # compute total energy in each frame
        energy = np.sum(pow_spec, 1)

        # handling zero enegies
        energy = handle_zero(energy)
        gfccs[:, 0] = np.log(energy)

    # liftering
    if lifter:
        gfccs = liftering(gfccs, lifter)

    return gfccs
