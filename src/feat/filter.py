import numpy as np

from feat.util import hz2mel, mel2hz
from feat.gfcc import gammatone_fbanks

SAMPLE_RATE = 16000
NUM_FFT = 512

# 6 filterbanks of phuong, computed using frequency table from report of Thomas
def get_fbank_thomas():
    # generate frequency bins
    freq = np.array([0, 303, 738, 1768, 3055, 4220, 5842, 8000])    # from Thomas's report
    
    bin = np.floor((NUM_FFT + 1) * freq / SAMPLE_RATE)

    # generate filterbanks
    fbank = np.zeros([6, NUM_FFT // 2 + 1])    # 6 x 257
    for j in range(0, 6):
        for i in range(int(bin[j]), int(bin[j + 1])):
            fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
        for i in range(int(bin[j + 1]), int(bin[j + 2])):
            fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
    return fbank


# 6 filterbanks of phuong, computed directly from filter 5 and filter 6. The filters from this function are the same as get_filterbanks_thomas
def get_fbank_phuong():
    # compute points evenly spaced in mels
    lowmel = hz2mel(0)
    highmel = hz2mel(SAMPLE_RATE / 2)

    ### new definition for filter banks. This definition fits with the idea in Hang-Phuong's Master thesis 
    melpoints_filter5 = np.linspace(lowmel, highmel, 7)   # filter 5
    melpoints_filter6 = np.linspace(lowmel, highmel, 8)    # filter 6

    melpoints = np.zeros(8)
    melpoints[0] = melpoints_filter6[0]
    melpoints[7] = melpoints_filter6[7]
    melpoints[1] = melpoints_filter6[1]
    melpoints[2] = melpoints_filter6[2]

    melpoints[3] = melpoints_filter5[3]
    melpoints[4] = melpoints_filter5[4]
    melpoints[5] = (melpoints_filter6[5] + melpoints_filter5[5])/2 
    melpoints[6] = (melpoints[5] + melpoints[7])/2

    freq = mel2hz(melpoints)

    # generate frequency bins
    bin = np.floor((NUM_FFT + 1) * freq / SAMPLE_RATE)

    # generate filterbanks
    fbank = np.zeros([6, NUM_FFT // 2 + 1])    # 6 x 257
    for j in range(0, 6):
        for i in range(int(bin[j]), int(bin[j + 1])):
            fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
        for i in range(int(bin[j + 1]), int(bin[j + 2])):
            fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
    return fbank


# same as phuong except melpoints[3] are updated
# def get_filterbanks_phuong_v2(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     # compute points evenly spaced in mels
#     lowmel = hz2mel(0)
#     highmel = hz2mel(sr / 2)

#     ### new definition for filter banks. This definition fits with the idea in Hang-Phuong's Master thesis 
#     melpoints_filter5 = np.linspace(lowmel, highmel, 7)   # filter 5
#     melpoints_filter6 = np.linspace(lowmel, highmel, 8)    # filter 6

#     melpoints = np.zeros(8)
#     melpoints[0] = melpoints_filter6[0]
#     melpoints[7] = melpoints_filter6[7]
#     melpoints[1] = melpoints_filter6[1]
#     melpoints[2] = melpoints_filter6[2]

#     melpoints[3] = (melpoints_filter6[3] + melpoints_filter5[3]) / 2
#     melpoints[4] = melpoints_filter5[4]
#     melpoints[5] = (melpoints_filter6[5] + melpoints_filter5[5]) / 2 
#     melpoints[6] = (melpoints[5] + melpoints[7])/2

#     freq = mel2hz(melpoints)

#     # generate frequency bins
#     bin = np.floor((num_fft + 1) * freq / sr)

#     # generate filterbanks
#     fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
#     for j in range(0, 6):
#         for i in range(int(bin[j]), int(bin[j + 1])):
#             fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
#         for i in range(int(bin[j + 1]), int(bin[j + 2])):
#             fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
#     return fbank


# same as phuong v2 except melpoints[4] are updated
# def get_filterbanks_phuong_v3(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     # compute points evenly spaced in mels
#     lowmel = hz2mel(0)
#     highmel = hz2mel(sr / 2)

#     ### new definition for filter banks. This definition fits with the idea in Hang-Phuong's Master thesis 
#     melpoints_filter5 = np.linspace(lowmel, highmel, 7)   # filter 5
#     melpoints_filter6 = np.linspace(lowmel, highmel, 8)    # filter 6

#     melpoints = np.zeros(8)
#     melpoints[0] = melpoints_filter6[0]
#     melpoints[7] = melpoints_filter6[7]
#     melpoints[1] = melpoints_filter6[1]
#     melpoints[2] = melpoints_filter6[2]

#     melpoints[3] = (melpoints_filter6[3] + melpoints_filter5[3]) / 2
#     melpoints[4] = (melpoints_filter6[4] + melpoints_filter5[4]) / 2
#     melpoints[5] = (melpoints_filter6[5] + melpoints_filter5[5]) / 2 
#     melpoints[6] = (melpoints[5] + melpoints[7])/2

#     freq = mel2hz(melpoints)

#     # generate frequency bins
#     bin = np.floor((num_fft + 1) * freq / sr)

#     # generate filterbanks
#     fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
#     for j in range(0, 6):
#         for i in range(int(bin[j]), int(bin[j + 1])):
#             fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
#         for i in range(int(bin[j + 1]), int(bin[j + 2])):
#             fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
#     return fbank


# same as phuong v3 except melpoints[3] are updated
# def get_filterbanks_phuong_v4(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     mel = np.array([0, 406, 811, 1217, 1758, 2198, 2519, 2840])
#     freq = mel2hz(mel)

#     # generate frequency bins
#     bin = np.floor((num_fft + 1) * freq / sr)

#     # generate filterbanks
#     fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
#     for j in range(0, 6):
#         for i in range(int(bin[j]), int(bin[j + 1])):
#             fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
#         for i in range(int(bin[j + 1]), int(bin[j + 2])):
#             fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
#     return fbank


# same as phuong v4 except melpoints[4--] are updated
# def get_filterbanks_phuong_v5(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     mel = np.array([0, 406, 811, 1217, 1858, 2180, 2510, 2840]) 
#     freq = mel2hz(mel)

#     # generate frequency bins
#     bin = np.floor((num_fft + 1) * freq / sr)

#     # generate filterbanks
#     fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
#     for j in range(0, 6):
#         for i in range(int(bin[j]), int(bin[j + 1])):
#             fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
#         for i in range(int(bin[j + 1]), int(bin[j + 2])):
#             fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
#     return fbank


# same as phuong v4 except melpoints[4--] are updated
# def get_filterbanks_phuong_v6(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     mel = np.array([0, 406, 811, 1217, 1823, 2198, 2519, 2840])
    
#     freq = mel2hz(mel)

#     # generate frequency bins
#     bin = np.floor((num_fft + 1) * freq / sr)

#     # generate filterbanks
#     fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
#     for j in range(0, 6):
#         for i in range(int(bin[j]), int(bin[j + 1])):
#             fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
#         for i in range(int(bin[j + 1]), int(bin[j + 2])):
#             fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
#     return fbank


# def get_filterbanks_phuong_v7(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filter_points = [  
#         [0,  406,  811],
#         [406, 811, 1217],
#         [811, 1217, 1623],
#         [811, 1420, 1893],
#         [1420, 1893, 2198],
#         [1893, 2198, 2519],
#         [2198, 2519, 2840],
#     ]

#     return get_custom_filterbanks(filter_mel_points=filter_points)


# def get_filterbanks_phuong_v8(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     mel = np.array([0, 406, 811, 1217, 1788, 2198, 2519, 2840])
#     freq = mel2hz(mel)

#     # generate frequency bins
#     bin = np.floor((num_fft + 1) * freq / sr)

#     # generate filterbanks
#     fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
#     for j in range(0, 6):
#         for i in range(int(bin[j]), int(bin[j + 1])):
#             fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
#         for i in range(int(bin[j + 1]), int(bin[j + 2])):
#             fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
#     return fbank


# def get_filterbanks_6mfcc_v2(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     # proposed mel points
#     mel = np.array([0, 406, 911, 1417, 1823, 2229, 2534, 2840])
#     freq = mel2hz(mel)
#     bin = np.floor((num_fft + 1) * freq / sr)

#     # generate filterbanks
#     fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
#     for j in range(0, 6):
#         for i in range(int(bin[j]), int(bin[j + 1])):
#             fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
#         for i in range(int(bin[j + 1]), int(bin[j + 2])):
#             fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
#     return fbank


# def get_filterbanks_6mfcc_v3(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     # proposed mel points
#     mel = np.array([0, 406, 811, 1420, 1823, 2162, 2501, 2840])
#     freq = mel2hz(mel)
#     bin = np.floor((num_fft + 1) * freq / sr)

#     # generate filterbanks
#     fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
#     for j in range(0, 6):
#         for i in range(int(bin[j]), int(bin[j + 1])):
#             fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
#         for i in range(int(bin[j + 1]), int(bin[j + 2])):
#             fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
#     return fbank


# Filter 1-2: same as 6 MFCC
# Filter 3 from hillen1995, F2 and F3 no dependency
# Fiter 4-6: divided in equal distance
# def get_filterbanks_6mfcc_v4(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filter_points = [  
#         [0,  406,  811],
#         [406, 811, 1217],
#         # [811, 1405, 1883], v1
#         [811, 1311, 1883],
#         [1405, 1883, 2202],
#         [1883, 2202, 2521],
#         [2202, 2521, 2840],
#     ]

#     return get_custom_filterbanks(filter_mel_points=filter_points)


# Filter 1-2: same as 6 MFCC
# Filter 3: the highest value is based on the average of maximum F2 of Hillen1995
# Fiter 4-6: divided in equal distance
# def get_filterbanks_6mfcc_v5(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filter_points = [  
#         [0,  406,  811],
#         [406, 811, 1217],
#         [811, 1341, 1788],
#         [1341, 1788, 2139],
#         [1788, 2139, 2489],
#         [2139, 2489, 2840],
#     ]

#     return get_custom_filterbanks(filter_mel_points=filter_points)


# def get_filterbanks_hillenbrand1995(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     # proposed mel points
#     mel = np.array([0, 277,  686, 1405, 1822, 2153, 2496, 2840])
#     freq = mel2hz(mel)
#     bin = np.floor((num_fft + 1) * freq / sr)

#     # generate filterbanks
#     fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
#     for j in range(0, 6):
#         for i in range(int(bin[j]), int(bin[j + 1])):
#             fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
#         for i in range(int(bin[j + 1]), int(bin[j + 2])):
#             fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
#     return fbank


# Filter 1-3 follow hillen1995 while the rest is divided in equal distance
# def get_filterbanks_hillenbrand1995_v2(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filter_points = [  
#         [0,  277,  343],
#         [335, 686, 1001],
#         [686, 1405, 1883],
#         [1405, 1764, 2122],
#         [1764, 2122, 2481],
#         [2122, 2481, 2840]
#     ]

#     return get_custom_filterbanks(filter_mel_points=filter_points)


# Filter 3 : merge between F3-F5
def get_fbank_8mfcc():
    filter_points = [  
        [0, 316, 631],
        # v1
        # [316, 631, 947],
        # [631, 1262, 1893],
        # v2
        # [316, 631, 1217],
        # [631, 1217, 1893],
        # v3
        [316, 631, 1262],
        [631, 1262, 1893],
        # v4
        # [316, 631, 1262],
        # [631, 1405, 1893],
        # v5
        # [316, 631, 1240],
        # [631, 1240, 1893],
        [1578, 1893, 2209],
        [1893, 2209, 2524],
        [2209, 2524, 2840],
    ]

    return get_fbank(filt_mel_points=filter_points)


#### trapezoidal filterbanks ####

# def get_filterbanks_trapezoidal_no(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filt_freq_points = [ 
#         [50, 80, 270, 300],
#         [300, 330, 720, 750],
#         [750, 800, 2400, 2450],
#         [2450, 2550, 3200, 3300],
#         [3300, 3400, 4100, 4200],
#         [4200, 4300, 5300, 5400],
#     ]

#     return get_fbank(sr=sr, num_fft=num_fft, filt_freq_points=filt_freq_points)


# def get_filterbanks_trapezoidal_no_v2(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filt_freq_points = [ 
#         [50, 275],
#         [275, 750],
#         [750, 2450],
#         [2450, 3300],
#         [3300, 4200],
#         [4200, 5500],
#     ]

#     return get_fbank(sr=sr, num_fft=num_fft, filt_freq_points=filt_freq_points, filt_stiff_ratio=0.1)


# def get_filterbanks_trapezoidal_v2(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filt_freq_points = [ 
#         [50, 100, 250, 300],
#         [250, 300, 900, 950],
#         [700, 1350, 2250, 2850],
#         [2200, 2400, 3150, 3400],
#         [3200, 3450, 4200, 4500],
#         [4050, 4400, 5050, 5400],
#     ]

#     return get_fbank(sr=sr, num_fft=num_fft, filt_freq_points=filt_freq_points)


# def get_filterbanks_trapezoidal_v13(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filt_freq_points = [ 
#         # v1
#         [50, 300],
#         [250, 850],
#         [700, 2650],
#         [2450, 3400],
#         [3300, 4300],
#         [4200, 5500],
#     ]

#     # v2
#     filt_stiff_ratio = [
#         0.1,
#         0.1,
#         0.2,
#         0.2,
#         0.2,
#         0.2,
#     ]

    # return get_fbank(sr=sr, num_fft=num_fft, filt_freq_points=filt_freq_points, filt_stiff_ratio=filt_stiff_ratio)


# def get_filterbanks_trapezoidal_v13_triangle(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filt_freq_points = [ 
#         [50, 300],
#         [250, 850],
#         [700, 2650],
#         [2450, 3400],
#         [3300, 4300],
#         [4200, 5500],
#     ]

#     return get_fbank(sr=sr, num_fft=num_fft, filt_freq_points=filt_freq_points, filt_stiff_ratio=1)


# def get_new_fbank(sr=16000, num_filt=6, num_fft=512, low_freq=0, high_freq=None):
#     filt_freq_points = [ 
#         [50, 75, 275, 350],
#         [200, 375, 850],
#         [500, 1300, 2450, 2900],
#         [2000, 2650, 3800],
#         [3000, 3700, 4900],
#         [3700, 4450, 5200, 5500],
#     ]

#     return get_fbank(sr=sr, num_fft=num_fft, filt_freq_points=filt_freq_points, filt_stiff_ratio=None)


# reduce overlap between f4 and f5
def get_new_fbank_v2():
    filt_freq_points = [ 
        [50, 75, 275, 350],
        [200, 375, 850],
        [500, 1300, 2450, 2900],
        [2000, 2650, 3800],
        [3000, 3700, 4700],
        [3900, 4450, 5200, 5500],
    ]

    return get_fbank(filt_freq_points=filt_freq_points, filt_stiff_ratio=None)


def get_new_fbank_v3():
    filt_freq_points = [ 
        # v3
        # [50, 100, 250, 350],
        # [200, 300, 550, 900],
        # [500, 1350, 2250, 3000],
        # [1900, 2400, 3150, 3650],
        # [3000, 3450, 4200, 4700],
        # [3700, 4400, 5050, 5500],

        # v4
        # v3 - use settings as TPZD v3
        # [50, 173, 350],
        # [200, 415, 900],
        # [500, 1759, 3000],
        # [1900, 2801, 3650],
        # [3000, 3811, 4700],
        # [3700, 4736, 5500],

        # v5 v2
        [0, 157, 350],
        [200, 537, 1000],
        [500, 1407, 3000],
        [1900, 2664, 3650],
        [3000, 3768, 4700],
        [3700, 4731, 6000],

        # v6
        # [75, 118, 220, 300],
        # [200, 360, 420, 600],
        # [700, 1575,	2200, 2750],
        # [2250, 2575, 2790, 3400],
        # [3200, 3650, 3850, 4550],
        # [3900, 4550, 5050, 5450],

        # v6 v2
        # [75, 100, 180, 300],
        # [220, 340, 420, 775],
        # [700, 1690, 2000, 2650],
        # [2250, 2600, 2800, 3400],
        # [3200, 3700, 3820, 4400],
        # [4000, 4620, 4950, 5400],

    
    ]

    return get_fbank(filt_freq_points=filt_freq_points, filt_stiff_ratio=None)


def get_new_fbank_v4():
    cnt_freqs = np.array([172, 814, 1758, 2801, 3801, 4735]) # v5v2
    # cnt_freqs = np.array([172, 914, 1758, 2801, 3801, 4735])

    factor = np.array([2, 2, 2, 1, 1, 1])

    fbanks, _ = gammatone_fbanks(cnt_freqs=cnt_freqs, factor=factor)
    return fbanks


    
####### Custom Filter #######


def get_fbank(sr=SAMPLE_RATE, num_fft=NUM_FFT, filt_freq_points=[], filt_mel_points=None, filt_stiff_ratio=None):
    if filt_mel_points:
        filt_freq_points = []
        for points in filt_mel_points:
            filt_freq_points.append([mel2hz(mel) for mel in points])

    fbank = np.zeros([len(filt_freq_points), num_fft // 2 + 1])

    for i, freq_points in enumerate(filt_freq_points):
        if len(freq_points) == 3:
            freq_bins = np.floor((num_fft + 1) * np.array(freq_points) / sr)
            
            for j in range(int(freq_bins[0]), int(freq_bins[1])):
                fbank[i, j] = (j - freq_bins[0]) / (freq_bins[1] - freq_bins[0])
            
            for j in range(int(freq_bins[1]), int(freq_bins[2])):
                fbank[i, j] = (freq_bins[2] - j) / (freq_bins[2] - freq_bins[1])
        else:
            if filt_stiff_ratio != None:

                if type(filt_stiff_ratio) == list:
                    stiff_ratio = filt_stiff_ratio[i]
                else:
                    stiff_ratio = filt_stiff_ratio

                if len(freq_points) == 4:
                    mel_points = [hz2mel(freq_points[0]), hz2mel(freq_points[3])]
                else:
                    mel_points = [hz2mel(freq_points[0]), hz2mel(freq_points[1])]

                stiff_length = int((mel_points[1] - mel_points[0]) * stiff_ratio / 2)
                stiff_1 = mel_points[0] + stiff_length
                stiff_2 = mel_points[1] - stiff_length

                freq_points = np.array([mel2hz(m) for m in [mel_points[0], stiff_1, stiff_2, mel_points[1]]])

            freq_bins = np.floor((num_fft + 1) * np.array(freq_points) / sr)

            for j in range(int(freq_bins[0]), int(freq_bins[1])):
                fbank[i, j] = (j - freq_bins[0]) / (freq_bins[1] - freq_bins[0])

            for j in range(int(freq_bins[1]), int(freq_bins[2])):
                fbank[i, j] = 1

            for j in range(int(freq_bins[2]), int(freq_bins[3])):
                fbank[i, j] = (freq_bins[3] - j) / (freq_bins[3] - freq_bins[2])

    return fbank
