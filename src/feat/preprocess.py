from typing import Tuple

import numpy as np
from feat.util import magnitude2power
from scipy.signal import lfilter
from spafe.utils import preprocessing


def pre_emphasis(sig: np.ndarray, coeff: float = 0.97) -> np.ndarray:
    sig = lfilter([1, -coeff], 1, sig)
    return sig


def ipre_emphasis(sig: np.ndarray, coeff: float = 0.97) -> np.ndarray:
    sig = lfilter([1], [1, -coeff], sig)
    return sig


def framing(sig: np.ndarray, 
            sr: int = 16000, 
            win_len: float = 0.025, 
            win_step: float = 0.01) -> Tuple[np.ndarray, int]:
    """
        Returns:
            numpy.ndarray: frames (num_frames x samples)
            int: frame length
    """
    return preprocessing.framing(sig, sr, win_len, win_step)


def iframing(frames: np.ndarray, sr: int = 16000, win_step: float = 0.01) -> np.ndarray:
    num_frame, frame_len = frames.shape
    frame_step = int(win_step * sr)

    sig_length = (num_frame - 1) * frame_step + frame_len
    sig = np.zeros(sig_length)

    indices = np.arange(num_frame) * frame_step
    frames_indices = np.arange(frame_len)
    frame_indexes = indices[:, None] + frames_indices

    # Sum the frames at their respective positions
    np.add.at(sig, frame_indexes.ravel(), frames.ravel())
    return sig


def windowing(frames: np.ndarray, frame_len: int, win_type: str = 'hamming') -> np.ndarray:
    win_func = {
        None: np.ones(frame_len),
        'hanning': np.hanning(frame_len),
        'bartlet': np.bartlett(frame_len),
        'kaiser': np.kaiser(frame_len, beta=14),
        'blackman': np.blackman(frame_len),
        'hamming': np.hamming(frame_len),
    }
    return win_func[win_type] * frames


def iwindowing(frames: np.ndarray, frame_len: int, win_type: str = 'hamming') -> np.ndarray:
    win_func = {
        None: np.ones(frame_len),
        'hanning': np.hanning(frame_len),
        'bartlet': np.bartlett(frame_len),
        'kaiser': np.kaiser(frame_len, beta=14),
        'blackman': np.blackman(frame_len),
        'hamming': np.hamming(frame_len),
    }
    return  frames / win_func[win_type]


def preprocess(
        sig: np.ndarray, 
        sr: int = 16000, 
        pre_emph: float = 0.97,
        win_len: float = 0.025, 
        win_step: float = 0.01,
        win_type: str = 'hamming') -> Tuple[np.ndarray, int]:
    """
        Returns:
            numpy.ndarray: frames (num_frames x samples)
            int: frame length
    """

    if pre_emph:
        sig = pre_emphasis(sig=sig,  coeff=pre_emph)

    frames, frame_len = framing(sig, sr, win_len, win_step)
    frames = windowing(frames, frame_len=frame_len, win_type=win_type)

    return frames, frame_len


def ipreprocess(
        frames: np.ndarray, 
        sr: int = 16000,
        pre_emph: float = 0.97,
        win_step: float = 0.01,
        win_type: str = 'hamming') -> np.ndarray:
    
    frames = iwindowing(frames, frames.shape[1], win_type)
    sig = iframing(frames, sr, win_step)

    if pre_emph:
        sig = ipre_emphasis(sig, pre_emph)

    return sig


def fft(frames: np.ndarray, num_fft: int = 512) -> np.ndarray:
    """
        Returns:
            numpy.ndarray: FFT in complex spec (num_frames x (num_fft / 2+1))
    """
    return np.fft.rfft(frames, num_fft)


def ifft(frames: np.ndarray, frame_len: int = None) -> np.ndarray:
    """
        Args:
            frame_len: lenght of the output frame
        Returns:
            numpy.ndarray: IFFT in complex spec (num_frames x frame_len)

    """
    return np.fft.irfft(frames, frame_len)


def fft_magnitude_spec(frames: np.ndarray, num_fft: int = 512) -> np.ndarray:
    """
        Returns:
            numpy.ndarray: FFT in magnitude spec (num_frames x (num_fft / 2+1))
    """
    return np.absolute(np.fft.rfft(frames, num_fft))


def fft_power_spec(frames: np.ndarray, num_fft: int = 512, power: int = 2, normalize: bool = True) -> np.ndarray:
    """
        Returns:
            numpy.ndarray: FFT in power spec (num_frames x (num_fft / 2+1))
    """
    return magnitude2power(fft_magnitude_spec(frames, num_fft), num_fft, power, normalize)


def get_fft_spec(ffts: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """
        Args:
            ffts        : FFT in complex spec (num_frames x (num_fft/2+1))
        Returns:
            magnitudes  : Magnitude of FFT component (num_frames x (num_fft / 2+1))
            phases      : Phase of FFT component (num_frames x (num_fft / 2+1))

    """
    magnitudes = np.abs(ffts)
    phases = np.angle(ffts)
    return magnitudes, phases


def get_recon_fft(magnitudes: np.ndarray, phases: np.ndarray) -> np.ndarray:
    """
        Args:
            magnitudes  : Magnitude of FFT component (num_frames x (num_fft / 2+1))
            phases      : Phase of FFT component (num_frames x (num_fft / 2+1))
        Returns:
            ffts        : Reconstructed FFT in complex spec (num_frames x (num_fft / 2+1))
    """
    recon_ffts = magnitudes * np.exp(1j * phases)
    return recon_ffts
