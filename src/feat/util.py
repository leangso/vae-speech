import numpy as np

from scipy import signal
from scipy.io import wavfile as wav
from scipy import fftpack

from spafe.utils.converters import (
    mel2hz,
    hz2mel,
    bark2hz,
    hz2bark,
    erb2hz,
    hz2erb,
)


def read_wav(file):
    sr, sig = wav.read(file)
    return sr, sig


def handle_zero(x: np.ndarray) -> np.ndarray:
    return np.where(x == 0, np.finfo(float).eps, x)


#### converter ####


def hz2x(x='mel'):
    return {
        'mel': hz2mel,
        'bark': hz2bark,
        'gamma': hz2erb,
    }[x]


def x2hz(x='mel'):
    return {
        'mel': mel2hz,
        'bark': bark2hz,
        'gamma': erb2hz,
    }[x]


def magnitude2power(mag_spec: np.ndarray, num_fft: int = 512, power: int = 2, normalize: bool =True) -> np.ndarray:
    """"
        power: 1 for energy, 2 for power
    """
    return 1.0 / num_fft * (mag_spec ** power) if normalize else mag_spec ** power


def power2decibel(power_spec: np.ndarray) -> np.ndarray:
    return 10 * np.log10(power_spec)


#### cepstral ####


def dct(frames: np.ndarray) -> np.ndarray:
    return fftpack.dct(x=frames, type=2, axis=1, norm="ortho")


def liftering(frames: np.ndarray, coef: int = 22) -> np.ndarray:
    if coef > 0:
        num_frame, num_coeff = np.shape(frames)
        n = np.arange(num_coeff)
        lift = 1 + (coef /2.) * np.sin(np.pi * n / coef)
        return lift * frames
    else:
        # values of L <= 0, do nothing
        return frames
    

def delta(frames: np.ndarray, win_size: int = 2) -> np.ndarray:
    padded = np.pad(frames, ((win_size, win_size), (0, 0)), mode='edge')

    denominator = 2 * sum([i ** 2 for i in range(1, win_size + 1)])
    filter = np.arange(-win_size, win_size + 1) / denominator

    delta = np.apply_along_axis(lambda c: np.correlate(c, filter, mode='valid'), axis=0, arr=padded)
    return delta


# delta by differenting between frames
def delta_diff(frames: np.ndarray, delta_step=1) -> np.ndarray:
    padded = np.pad(frames, ((delta_step, 0), (0, 0)), mode='edge')
    delta = padded[delta_step:] - padded[:-delta_step]
    return delta


# delta proposed by Phuong
def delta_phuong(frames: np.ndarray, win_len=0.025) -> np.ndarray:
    padded = np.pad(frames, ((2, 2), (0, 0)), mode='edge')
    coeff = np.array([1, -0.8, 0, 0.8, -1]) # coefficient of each contexts

    delta = np.empty_like(frames)
    for t in range(len(frames)):
        delta[t] = np.dot(coeff, padded[t: t + 2 * 2 + 1]) / (win_len * 1000)
    return delta


def cmn(frames: np.ndarray) -> np.ndarray:
    mean = np.mean(frames, axis=0)
    return frames - mean


def cvn(frames: np.ndarray) -> np.ndarray:
    mean = np.mean(frames, axis=0)
    std = handle_zero(np.std(frames, axis=0))
    return (frames - mean) / std


#### normalize ####


def back_average(x: np.ndarray, win_len: int = 3) -> np.ndarray:
    padded = np.pad(x, ((win_len - 1, 0), (0, 0)), mode='edge')
    back_average = np.apply_along_axis(lambda c: np.convolve(c, np.ones(win_len) / win_len, mode='valid'), axis=0, arr=padded)
    return back_average


def average(x: np.ndarray, win_len: int = 3) -> np.ndarray:
    padded = np.pad(x, ((win_len // 2, win_len - 1 - win_len // 2), (0, 0)), mode='edge')
    average = np.apply_along_axis(lambda c: np.convolve(c, np.ones(win_len) / win_len, mode='valid'), axis=0, arr=padded)
    return average


def zscore(x: np.ndarray) -> np.ndarray:
    return (x - x.mean()) / x.std()


def semitone(x: np.ndarray, ref: int = 100) -> np.ndarray:
    x = handle_zero(x)
    return (12 / np.log10(2)) * np.log10(x / ref)


def semitone_avg(x: np.ndarray) -> np.ndarray:
    return semitone(x, np.mean(x))


def savgol_filter(x: np.ndarray, win_len: int, poly_order: int) -> np.ndarray:
    return signal.savgol_filter(x, window_length=win_len, polyorder=poly_order)
