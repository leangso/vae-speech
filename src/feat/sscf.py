import numpy as np

from feat.preprocess import preprocess, fft_magnitude_spec, fft_power_spec
from feat.util import handle_zero, dct
from feat.mfcc import mel_fbanks


def sscfs(
    sig: np.ndarray,
    num_filt: int = 6,
    num_fft: int = 512,
    sr: int = 16000,
    pre_emph: float = 0.97,
    win_len: float = 0.025, 
    win_step: float = 0.01,
    win_type: str = "hamming",
    low_freq: float = 0,
    high_freq: float = None,
    fbanks: np.ndarray = None,
) -> np.ndarray:
    """
        Returns:
            numpy.ndarray: Spectral Subband Centroid Frequencies - SSCFs (num_frames x num_ceps)
    """

    # get fbanks
    if fbanks is None:
        fbanks, _ = mel_fbanks(
            num_filt=num_filt,
            num_fft=num_fft,
            sr=sr,
            low_freq=low_freq,
            high_freq=high_freq,
        )

    # preprocess
    frames, _ = preprocess(sig, sr, pre_emph, win_len, win_step, win_type)

    # power spec
    pow_spec = fft_power_spec(frames, num_fft)    
    pow_spec = handle_zero(pow_spec)

    # compute filterbank energies
    feat = np.dot(pow_spec, fbanks.T)  

    # compute centroids
    R = np.tile(np.linspace(1, sr / 2, np.size(pow_spec, 1)), (np.size(pow_spec, 0), 1))
    sscfs = np.dot(pow_spec * R, fbanks.T) / feat

    return sscfs


def sscms(
    sig: np.ndarray,
    num_filt: int = 6,
    num_fft: int = 512,
    sr: int = 16000,
    pre_emph: float = 0.97,
    win_len: float = 0.025, 
    win_step: float = 0.01,
    win_type: str = "hamming",
    low_freq: float = 0,
    high_freq: float = None,
    fbanks: np.ndarray = None,
) -> np.ndarray:
    """
        Returns:
            numpy.ndarray: Spectral Subband Centroid Magnitudes - SSCMs (num_frames x num_ceps)
    """

    # get fbanks
    if fbanks is None:
        fbanks, _ = mel_fbanks(
            num_filt=num_filt,
            num_fft=num_fft,
            sr=sr,
            low_freq=low_freq,
            high_freq=high_freq,
        )
    
    # preprocess
    frames, _ = preprocess(sig, sr, pre_emph, win_len, win_step, win_type)

    # magnitude spec
    mag_spec = fft_magnitude_spec(frames, num_fft)
    mag_spec = handle_zero(mag_spec)

    rectangular_fbanks = np.where(fbanks != 0, 1, fbanks)

    # compute magnitude
    R = np.tile(np.linspace(1, sr / 2, np.size(mag_spec, 1)), (np.size(mag_spec, 0), 1))
    sscms = np.dot(mag_spec * R, fbanks.T) / np.dot(R, rectangular_fbanks.T)
    sscms = handle_zero(sscms)

    sscms = np.log(sscms)

    sscms = dct(sscms, type=2, axis=1, norm='ortho')

    return sscms


def hfpes(
    sig: np.ndarray,
    num_fft: int = 512,
    sr: int = 16000,
    pre_emph: float = 0.97,
    win_len: float = 0.025, 
    win_step: float = 0.01,
    win_type: str = "hamming",
    threshold: int = 2000
) -> np.ndarray:
    """
        Returns:
            numpy.ndarray: High Frequency Power Energy - hfpe (num_frames x (num_fft/2+1))
    """

    # preprocess
    frames, _ = preprocess(sig, sr, pre_emph, win_len, win_step, win_type)

    # compute power spectrum
    pow_spec = fft_power_spec(frames, num_fft)
    
    freq = np.linspace(1, sr / 2, np.size(pow_spec, 1))
    freq = np.where(freq < threshold, 0, freq)
    freq = np.where(freq >= threshold, 1, freq)
    freq = np.expand_dims(freq, axis=0)

    hfpes = np.dot(pow_spec, freq.T)
    hfpes = handle_zero(hfpes)

    hfpes = np.log(hfpes)

    return hfpes
