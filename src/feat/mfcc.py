import numpy as np

from typing import Tuple
from feat.preprocess import preprocess, fft_magnitude_spec
from feat.util import handle_zero, dct, liftering, magnitude2power
from feat.util import hz2mel, mel2hz


def mel_fbanks(
    num_filt: int = 40,
    num_fft: int = 512,
    sr: int = 16000,
    low_freq: float = 0,
    high_freq: float = None,
) -> Tuple[np.ndarray, np.ndarray]:
    """
        Returns:
            tuple:
                - numpy.ndarray : array of size num_filts * (num_fft/2 + 1) containing filter bank. Each row holds 1 filter.
                - numpy.ndarray : array of center frequencies in Mel.
    """

    high_freq = high_freq or sr / 2
    assert high_freq <= sr / 2, "highfreq is greater than samplerate/2"

    # compute points evenly spaced in mels
    low_mel = hz2mel(low_freq)
    high_mel = hz2mel(high_freq)

    mel_points = np.linspace(low_mel , high_mel, num_filt + 2)
    # our points are in Hz, but we use fft bins, so we have to convert
    #  from Hz to fft bin number
    freq_bins = np.floor((num_fft + 1) * mel2hz(mel_points) / sr)

    fbanks = np.zeros([num_filt, num_fft // 2+1])
    for j in range(0, num_filt):
        for i in range(int(freq_bins[j]), int(freq_bins[j+1])):
            fbanks[j, i] = (i - freq_bins[j]) / (freq_bins[j+1] - freq_bins[j])

        for i in range(int(freq_bins[j+1]), int(freq_bins[j+2])):
            fbanks[j, i] = (freq_bins[j+2] - i) / (freq_bins[j+2] - freq_bins[j+1])

    return fbanks, mel_points


def mel_spectrogram(
    sig: np.ndarray,
    num_filt: int = 40,
    num_fft: int = 512,
    sr: int = 16000,
    pre_emph: float = 0.97,
    win_len: float = 0.025, 
    win_step: float = 0.01,
    win_type: str = "hamming",
    low_freq: float = 0,
    high_freq: float = None,
    power: int = 2,
    norm_power: bool = True,
    fbanks: np.ndarray = None,
) -> Tuple[np.ndarray, np.ndarray]:
    """
        Args:
            power: 1 for energy, 2 for power
    
        Returns:
            tuple:
                - numpy.ndarray: the mel spectrogram (num_frames x num_filts)
                - numpy.ndarray: the FFT in power spec.
    """

    # get fbanks
    if fbanks is None:
        fbanks, _ = mel_fbanks(
            num_filt=num_filt,
            num_fft=num_fft,
            sr=sr,
            low_freq=low_freq,
            high_freq=high_freq,
        )

    # preprocess
    frames, _ = preprocess(sig, sr, pre_emph, win_len, win_step, win_type)

    # FFT -> |FFT|
    mag_spec = fft_magnitude_spec(frames, num_fft)

    # power spectrum
    pow_spec = magnitude2power(mag_spec, num_fft, power, norm_power)

    # mel spectrogram
    mel_spec = np.dot(pow_spec, fbanks.T)

    return mel_spec, pow_spec


def mfccs(
    sig: np.ndarray,
    num_cep: int = 13,
    num_filt: int = 40,
    num_fft: int = 512,
    sr: int = 16000,
    pre_emph: float = 0.97,
    win_len: float = 0.025, 
    win_step: float = 0.01,
    win_type: str = "hamming",
    low_freq: float = 0,
    high_freq: float = None,
    use_energy: bool = False,
    lifter: int = 22,
    fbanks: np.ndarray = None,
) -> np.ndarray:
    """
        Returns:
            numpy.ndarray: MFCCs (num_frames x num_ceps)
    """

    # run checks
    if num_filt < num_cep:
        raise Exception("num_filts must be >= num_ceps")
    
    # compute mel spectrogram
    mel_spec, pow_spec = mel_spectrogram(
        sig=sig, 
        num_filt=num_filt, 
        num_fft=num_fft, 
        sr=sr, 
        pre_emph=pre_emph, 
        win_len=win_len, 
        win_step=win_step, 
        win_type=win_type, 
        low_freq=low_freq, 
        high_freq=high_freq, 
        fbanks=fbanks
    )

    # log
    mel_spec = handle_zero(mel_spec)
    mel_spec = np.log(mel_spec)

    # DCT
    mfccs = dct(mel_spec)[:, :num_cep]

    # use energy for 1st features column
    if use_energy:
        # compute total energy in each frame
        energy = np.sum(pow_spec, 1)

        # handling zero enegies
        energy = handle_zero(energy)
        mfccs[:, 0] = np.log(energy)

    # liftering
    if lifter:
        mfccs = liftering(mfccs, lifter)

    return mfccs
