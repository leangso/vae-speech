import os
import shutil
import sys

import torch

sys.path.insert(0, '/home/leangso/work/vae-speech/src')
from os import path

import soundfile as sf
import util.audio as audio
import util.file as file

#####


def load_model(exp_dir, ckpt_file, config_file, model_class):
    ckpt_file = path.join(exp_dir, 'checkpoint', chpt_file)
    config_file = path.join(exp_dir, 'config', config_file)

    model = file.load_model(model_class, ckpt_file=ckpt_file, hyps_file=config_file)
    model.eval()

    ckpt = torch.load(ckpt_file)
    epoch = ckpt['epoch']
    step = ckpt['global_step']

    return model, epoch, step


def generate_speech(records, output_dir, model, length=None, use_spk_id=True):
    os.makedirs(output_dir, exist_ok=True)

    for idx, record in enumerate(records):
        audio_file = record[0]
        spk_id = audio_file.split('/')[-2]
        
        trans_id = audio_file.split('/')[-1]
        utt_id = '%s-%s' % (spk_id, trans_id)

        spk_id_mapping = record[1]
        
        print('--> Process wav file: ', audio_file)
        print('spk:', spk_id)
        print('id mapping:', spk_id_mapping)
        
        sig, sr = audio.load_audio(audio_file)

        chunk = sig[:length] if length is not None else sig

        x = torch.tensor(chunk).float().unsqueeze(0)
        spk_id = torch.tensor([spk_id_mapping]).long()

        if use_spk_id:
            x_hat = model.generate(x, spk_id)
        else:
            x_hat = model.generate(x)

        sf.write('%s/recon_%d_%d_%s' % (output_dir, idx, spk_id, utt_id), x_hat, sr, subtype='PCM_16')

        print()


recon_records = [
        # train
        ['/home/leangso/work/exp/data/french_v3/ds/wav/HBJ_Train/SBJS4160010-0007.wav', 61],
        ['/home/leangso/work/exp/data/french_v3/ds/wav/HAK_Train/SAKS6760006-0003.wav', 55],
        ['/home/leangso/work/exp/data/french_v3/ds/wav/HZS_Train/SZSS2200001-0005.wav', 99],
        
        ['/home/leangso/work/exp/data/french_v3/ds/wav/FEA_Train/SEAS2920007-0005.wav', 23],
        ['/home/leangso/work/exp/data/french_v3/ds/wav/FAM_Train/SAMS8260002-0002.wav', 7],
        ['/home/leangso/work/exp/data/french_v3/ds/wav/FAF_Train/SAFS5970004-0008.wav', 4],

        # test
        ['/home/leangso/work/exp/data/french_v3/ds/wav/HCE_Test/SCES330000-0000.wav', 67],
        ['/home/leangso/work/exp/data/french_v3/ds/wav/HFY_Test/SFYS950008-0009.wav', 72],
        ['/home/leangso/work/exp/data/french_v3/ds/wav/HBL_Test/PBLP00000-0000.wav', 62],

        ['/home/leangso/work/exp/data/french_v3/ds/wav/FBC_Test/SBCS660001-0000.wav', 12],
        ['/home/leangso/work/exp/data/french_v3/ds/wav/FMB_Test/SMBS130002-0002.wav', 32],
        ['/home/leangso/work/exp/data/french_v3/ds/wav/FAA_Test/PAAP00000-0000.wav', 0],
    ]


convert_records = [
    # train
    ['/home/leangso/work/exp/data/french_v3/ds/wav/HBJ_Train/SBJS4160010-0007.wav', 55],
    ['/home/leangso/work/exp/data/french_v3/ds/wav/HBJ_Train/SBJS4160010-0007.wav', 23],

    ['/home/leangso/work/exp/data/french_v3/ds/wav/FEA_Train/SEAS2920007-0005.wav', 61],
    ['/home/leangso/work/exp/data/french_v3/ds/wav/FEA_Train/SEAS2920007-0005.wav', 7],

    ['/home/leangso/work/exp/data/french_v3/ds/wav/HCE_Test/SCES330000-0000.wav', 55],
    ['/home/leangso/work/exp/data/french_v3/ds/wav/HCE_Test/SCES330000-0000.wav', 23],

    ['/home/leangso/work/exp/data/french_v3/ds/wav/FBC_Test/SBCS660001-0000.wav', 61],
    ['/home/leangso/work/exp/data/french_v3/ds/wav/FBC_Test/SBCS660001-0000.wav', 7],
     
    # test
    ['/home/leangso/work/exp/data/french_v3/ds/wav/HCE_Test/SCES330000-0000.wav', 61],
    ['/home/leangso/work/exp/data/french_v3/ds/wav/HCE_Test/SCES330000-0000.wav', 23],

    ['/home/leangso/work/exp/data/french_v3/ds/wav/HFY_Test/SFYS950008-0009.wav', 61],
    ['/home/leangso/work/exp/data/french_v3/ds/wav/HFY_Test/SFYS950008-0009.wav', 23],

    ['/home/leangso/work/exp/data/french_v3/ds/wav/FBC_Test/SBCS660001-0000.wav', 61],
    ['/home/leangso/work/exp/data/french_v3/ds/wav/FBC_Test/SBCS660001-0000.wav', 23],

    ['/home/leangso/work/exp/data/french_v3/ds/wav/FMB_Test/SMBS130002-0002.wav', 61],
    ['/home/leangso/work/exp/data/french_v3/ds/wav/FMB_Test/SMBS130002-0002.wav', 23],
]


if __name__ == "__main__":

    from model.vq_vae import VQVAE

    exp_dir = '/home/leangso/work/exp/result-vae/french_v3/exp001'
    chpt_file = 'last.ckpt'
    config_file = 'model.yaml'
    model_class = VQVAE

    output_dir = '/home/leangso/work/exp/result-vae-eval'
    length = None
    use_spk_id = True
    clean = True

    model, epoch, step = load_model(exp_dir, chpt_file, config_file, model_class)

    model_output_dir = path.join(output_dir, '%s-%s-%s' % (os.path.basename(exp_dir), epoch, step))
    if clean is True:
        shutil.rmtree(model_output_dir, ignore_errors=True)

    print('---> Reconstruct speech\n')
    recon_dir = path.join(model_output_dir, 'recon_wav')
    generate_speech(recon_records, recon_dir, model, length=length, use_spk_id=use_spk_id)

    print('\n---> Convert speech\n')
    convert_dir = path.join(model_output_dir, 'convert_wav')
    generate_speech(convert_records, convert_dir, model, length=length, use_spk_id=use_spk_id)
