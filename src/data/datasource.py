import os
from os import path
from typing import Union

import torch
from data.dataset import SpeechDataset, get_utt_id_info
from experiment.experiment import DataSource
from feat.feat import extract_features
from torch.utils.data import DataLoader
from util import get, mulaw_encode
from util.audio import chunk_signal, load_audio, trim_signal
from util.file import read_yaml_file, save_json_file, scan_dir
from util.logger import get_logger

logger = get_logger(__name__)


def prepare_spk_dict(data_dir: str):
    wav_dir = path.join(data_dir, 'wav')

    spk_ids = []
    for spk_dir in scan_dir(wav_dir):
        if not spk_dir.is_dir() or spk_dir.name.startswith('.') or spk_dir.name.startswith('_'):
            continue

        spk_ids.append(spk_dir.name)

    spk_ids = sorted([spk for spk in spk_ids])
    spk_dict = {spk_id: i for i, spk_id in enumerate(spk_ids)}

    return spk_dict


class SpeechDataSource(DataSource):

    def __init__(
            self, 
            data_dir: str, 
            train_set: str, 
            test_sets: Union[str, list], 
            output_dir: str,
            batch_size: int = 20,
            read_size: int = None,
            num_worker: int = 5,
            skip_utt_file: str = None,
            **feat_config):
        
        super(SpeechDataSource, self).__init__()
        self.data_dir = data_dir
        self.train_set = train_set
        self.test_sets = [test_sets] if type(test_sets) is not list else test_sets
        self.output_dir = output_dir
        self.batch_size = batch_size
        self.read_size = read_size
        self.num_worker = num_worker
        self.skip_utt_file = skip_utt_file
        self.feat_config = feat_config

        if num_worker is None:
            num_worker = int(os.cpu_count() * 4/5)
            logger.debug(f'Set number of worker (4/5): {num_worker}')

        self.spk_dict = prepare_spk_dict(data_dir)
        self.save_spk_dict()

        self.init_dataloader()

    def init_dataloader(self):
        logger.debug(f'Init train data loader: {self.data_dir}/{self.train_set}')
        self.train = DataLoader(
            SpeechDataset(
                data_dir=self.data_dir, 
                dataset_name=self.train_set, 
                output_dir=self.output_dir, 
                preprocess=self.process, 
                get_item=self.get_item, 
                size=self.read_size,
                skip_utt_file=self.skip_utt_file
            ), 
            batch_size=self.batch_size,
            num_workers=self.num_worker,
            collate_fn=self.collate_batch,
            shuffle=True,
        )

        self.test = []
        for dataset in self.test_sets:
            logger.debug(f'Init test data loader: {self.data_dir}/{dataset}')
            dataloader = DataLoader(
                SpeechDataset(
                    data_dir=self.data_dir, 
                    dataset_name=dataset, 
                    output_dir=self.output_dir, 
                    preprocess=self.process, 
                    get_item=self.get_item, 
                    size=self.read_size,
                    skip_utt_file=self.skip_utt_file
                ), 
                batch_size=self.batch_size,
                num_workers=self.num_worker,
                collate_fn=self.collate_batch
            )
            self.test.append(dataloader)

    def process(self, utt_id, trans, wav_file):
        spk_id, session_id, trans_id = get_utt_id_info(utt_id)

        signal, _ = load_audio(wav_file, sr=self.feat_config['sample_rate'], normalize=self.feat_config['normalize'])

        mulaw_size = self.feat_config['mulaw_size']
        if mulaw_size is not None:
            signal = mulaw_encode(signal, mu=mulaw_size)

        map_spk_id = self.spk_dict[spk_id]

        data_dict = {}
        if self.feat_config['chunk_size'] is None:
            data = [map_spk_id, self.extract_features(signal)]
            if self.feat_config['raw_enable']:
                data.append(signal)
            
            data_dict[utt_id] = data
        else:
            chunks = chunk_signal(signal, chunk_len=self.feat_config['chunk_size'])
            for idx, chunk in enumerate(chunks):
                key = '%s-%s' % (utt_id, idx)

                data = [map_spk_id, self.extract_features(chunk)]
                if self.feat_config['raw_enable']:
                    data.append(chunk)

                data_dict[key] = data
    
        return data_dict

    def extract_features(self, signal):
        feats = extract_features(signal, **self.feat_config)
        return feats

    def save_spk_dict(self):
        spk_dict_file = path.join(self.output_dir, 'spk_dict.json')
        save_json_file(spk_dict_file, self.spk_dict)        

    def collate_batch(self, batch):
        output = ()
        for data in zip(*batch):
            output = output + (torch.tensor(data),)
        return output
    
    def get_item(self, data):
        return data

    def train_dataloader(self):
        return self.train

    def test_dataloader(self):
        return self.test

    @staticmethod
    def from_config(config: dict):
        data_dir = get(config, 'data_dir')
        train_set = get(config, 'train_set')
        test_sets = get(config, 'test_sets')
        output_dir = get(config, 'output_dir')
        batch_size = get(config, 'batch_size', 1)
        read_size = get(config, 'read_size')
        num_worker = get(config, 'num_worker', 1)
        skip_utt_file = get(config, 'skip_utt_file')

        feat_config = read_yaml_file(config['feat_file'])

        return SpeechDataSource(
            data_dir=data_dir, 
            train_set=train_set, 
            test_sets=test_sets, 
            output_dir=output_dir,
            batch_size=batch_size,
            read_size=read_size,
            num_worker=num_worker,
            skip_utt_file=skip_utt_file,
            **feat_config
        )


class SamplingSpeechDataSource(SpeechDataSource):

    def __init__(
            self, 
            data_dir: str, 
            train_set: str, 
            test_sets: Union[str, list], 
            output_dir: str,
            batch_size: int = 20, 
            read_size: int = None,
            num_worker: int = 5,
            skip_utt_file: str = None,
            **feat_config):
        
        super(SamplingSpeechDataSource, self).__init__(
            data_dir, 
            train_set, 
            test_sets, 
            output_dir,
            batch_size,
            read_size, 
            num_worker,
            skip_utt_file,
            **feat_config
        )

    def process(self, utt_id, trans, wav_file):
        spk_id, session_id, trans_id = get_utt_id_info(utt_id)

        signal, _ = load_audio(wav_file, sr=self.feat_config['sample_rate'], normalize=self.feat_config['normalize'])

        mulaw_size = self.feat_config['mulaw_size']
        if mulaw_size is not None:
            signal = mulaw_encode(signal, mu=mulaw_size)

        map_spk_id = self.spk_dict[spk_id]

        data_dict = {}        
        data_dict[utt_id] = [map_spk_id, signal]

        return data_dict

    def get_item(self, data):
        chunk_size = self.feat_config.get('chunk_size')

        map_spk_id, signal = data[0], data[1]

        if chunk_size is not None:
            signal = trim_signal(signal, chunk_size, random_trim=True)

        feats = self.extract_features(signal)

        return [map_spk_id, feats, signal]

    @staticmethod
    def from_config(config: dict):
        data_dir = get(config, 'data_dir')
        train_set = get(config, 'train_set')
        test_sets = get(config, 'test_sets')
        output_dir = get(config, 'output_dir')
        batch_size = get(config, 'batch_size', 1)
        read_size = get(config, 'read_size')
        num_worker = get(config, 'num_worker', 1)
        skip_utt_file = get(config, 'skip_utt_file')

        feat_config = read_yaml_file(config['feat_file'])

        return SamplingSpeechDataSource(
            data_dir=data_dir, 
            train_set=train_set, 
            test_sets=test_sets, 
            output_dir=output_dir,
            batch_size=batch_size,
            read_size=read_size,
            num_worker=num_worker,
            skip_utt_file=skip_utt_file,
            **feat_config
        )