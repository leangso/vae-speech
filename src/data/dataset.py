import os
from os import path

import torch
from kaldiio import load_scp, save_ark
from tqdm import tqdm
from util.logger import get_logger

logger = get_logger(__name__)


def get_utt_id_info(utt_id: str):
    utt_parts = utt_id.split('-')
    if len(utt_parts) == 3:
        # spk_id-session_id-trans_id
        spk_id, session_id, trans_id = utt_parts[0], utt_parts[1], utt_parts[2]
    else: 
        # spk_id-trans_id
        spk_id, session_id, trans_id = utt_parts[0], None, utt_parts[1]

    return spk_id, session_id, trans_id


def preprocess(utt_id, trans, wav_file):
    return {utt_id: (utt_id, trans, wav_file)}


def prepare_id_mapping(scp_reader):
    key_mapping = {}
    for key, _ in scp_reader.items():
        key_mapping[len(key_mapping)] = key
    return key_mapping


class SpeechDataset(torch.utils.data.Dataset):

    def __init__(
        self, 
        data_dir: str, 
        dataset_name: str,
        output_dir: str,
        preprocess: preprocess,
        skip_utt: list = [], 
        size: int = None,
        get_item = None,
        skip_utt_file: str = None):
        
        self.data_dir = data_dir
        self.dataset_name = dataset_name
        self.output_dir = output_dir
        self.preprocess = preprocess
        self.size = size
        self.get_item = get_item
        self.skip_utt_file = skip_utt_file
    
        self.skip_utt = self.load_skip_utt()
        self.skip_utt.extend(skip_utt)

        self.process()

    def load_skip_utt(self):
        skips = []

        skip_file = path.join(self.data_dir, self.skip_utt_file if self.skip_utt_file is not None else 'skip.txt')
        if os.path.exists(skip_file):
            with open(skip_file, 'r', encoding='utf-8') as reader:
                for line in reader:
                    utt_id = line.strip().split('\t', 1)[0]
                    skips.append(utt_id)

            logger.debug(f'Skip utterances: {len(skips)}')
        else:
            logger.debug(f'Skip utterance file not found: {skip_file}')
    
        return skips

    def process(self):
        if not os.path.exists(self.data_dir):
            raise Exception('Data directory not found: ', self.data_dir)

        trans_file = os.path.join(self.data_dir, self.dataset_name + '.csv')
        if not os.path.exists(trans_file):
            raise Exception('Transcription file not found: ', trans_file)

        dataset_dir = path.join(self.output_dir, self.dataset_name)
        os.makedirs(dataset_dir, exist_ok=True)

        ark_file = path.join(dataset_dir, 'feats.ark')
        scp_file = path.join(dataset_dir, 'feats.scp')

        if path.exists(ark_file) and path.exists(scp_file):
            logger.debug(f'Loading preproceeded data from: {scp_file}')
            self.data = load_scp(scp_file)
            self.key_mapping = prepare_id_mapping(self.data)
        else:
            logger.debug(f'Processing data: {self.data_dir}/{self.dataset_name}')
            wav_dir = path.join(self.data_dir, 'wav')

            wav_scp_file = path.join(dataset_dir, 'wav.scp')
            wav_scp_writer = open(wav_scp_file, 'w', encoding='utf-8')

            self.key_mapping = {}
            with open(trans_file, mode='r', encoding='utf-8') as reader:
                for idx, line in tqdm(enumerate(reader)):                
                    if self.size is not None and idx > self.size:
                        break

                    # get description of sample
                    line_eles = line.split('\t', 1)
                    utt_id, trans = line_eles[0], line_eles[1].strip()
            
                    if utt_id.startswith('#') or utt_id in self.skip_utt:
                        continue
                    
                    spk_id, session_id, trans_id = get_utt_id_info(utt_id)
                    if session_id is not None:
                        wav_file = path.join(wav_dir, spk_id, '%s-%s.wav' % (session_id, trans_id))
                    else: 
                        wav_file = path.join(wav_dir, spk_id, '%s.wav' % (trans_id))

                    data_dict = self.preprocess(utt_id, trans, wav_file)
                    for idx, (key, data)in enumerate(data_dict.items()):
                        self.key_mapping[len(self.key_mapping)] = key

                        # feat ark and scp
                        save_ark(ark=ark_file, array_dict={key: data}, scp=scp_file, write_function='pickle', append=True)

                    # wav.scp
                    wav_scp_writer.write('%s %s\n' % (utt_id, wav_file))

                wav_scp_writer.close()

                self.data = load_scp(scp_file)
    
        logger.debug(f'Dataset size: {len(self.key_mapping)}')

    def __len__(self):
        return len(self.key_mapping)

    def __getitem__(self, idx):
        key = self.key_mapping[idx]
        data = self.data[key]

        if self.get_item is not None and callable(self.get_item):
            data = self.get_item(data)
    
        return data
