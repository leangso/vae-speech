import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from module import Conv, Linear, get_gru_cell
from module.feat import Fbank
from module.jitter import Jitter
from torch.distributions import Categorical
from tqdm import tqdm
from util import get, mulaw_decode
from vector_quantize_pytorch import VectorQuantize


class Encoder(nn.Module):

    def __init__(self, **config):
        super(Encoder, self).__init__()

        #######
        filter_shape = get(config, 'filter_shape', 'gaussian')
        win_length = get(config, 'win_length', 20)
        hop_length = get(config, 'hop_length', 10)
        num_filt = get(config, 'num_filt', 6)
        num_fft = get(config, 'num_fft', 512)
        sample_rate = get(config, 'sample_rate', 16000)
        min_freq = get(config, 'min_freq', 50)
        fc_ranges = get(config, 'fc_ranges', None)
        delta = get(config, 'delta', False)

        hidden_channel = get(config, 'hidden_channel', 768)
        quantize_size = get(config, 'quantize_size')
        quantize_channel = get(config, 'quantize_channel', 64)
        commitment_cost = get(config, 'commitment_cost', 0.25)
        decay = get(config, 'decay', 0.99)
        epsilon = get(config, 'epsilon', 1e-5)

        if fc_ranges is not None and len(fc_ranges) < num_filt:
            for i in range(num_filt - len(fc_ranges)):
                fc_ranges.append(None)

        self.fbank = Fbank(
            win_length=win_length,
            hop_length=hop_length,
            filter_shape=filter_shape,
            n_mels=num_filt, 
            n_fft=num_fft,
            sample_rate=sample_rate, 
            f_min=min_freq,
            fc_ranges=fc_ranges,
            param_change_factor=1.0,
            param_rand_factor=0.0,
            requires_grad=True,
            deltas=delta
        )
        
        self.encoder = nn.Sequential(
            Conv(num_filt * 3 if delta is True else num_filt, hidden_channel, 3, 1, 1),
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),

            Conv(hidden_channel, hidden_channel, 3, 1, 1),
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),
            
            Conv(hidden_channel, hidden_channel, 4, 2, 2), # downsample by 2
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),

            Conv(hidden_channel, hidden_channel, 3, 1, 1),
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),

            Conv(hidden_channel, hidden_channel, 3, 1, 1),
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),

            Conv(hidden_channel, quantize_channel, 1, 1) # project output channel to embedding dimension
        )

        self.quantizer = VectorQuantize(
            dim=quantize_channel,
            codebook_size=quantize_size,     
            codebook_dim=quantize_channel,
            commitment_weight=commitment_cost,
            decay=decay,             
            eps=epsilon
        )

        self.quantize_size = quantize_size

    def forward(self, x):
        """"
            Args:
                x           : B, T - values in [-1, 1]

            Outputs:
                z           : B, Q (quantize_channel), L
                vq_loss     :
                perplexity  :
        """
        h = self.fbank(x).transpose(1, 2) # B, C, L
        h = self.encoder(h) # B, C, L

        # vq
        z, indice, vq_loss = self.quantizer(h.transpose(1, 2)) # B, L, Q
        z = z.transpose(1, 2) # B, Q, L

        # perplexity & active code
        encoding = F.one_hot(indice.reshape(-1), self.quantize_size).float()
        avg_prob = torch.mean(encoding, dim=0)
        perplexity = torch.exp(-torch.sum(avg_prob * torch.log(avg_prob + 1e-10)))

        active = indice.unique().numel() / self.quantize_size * 100

        return z, vq_loss, perplexity, active

    def encode(self, x):
        """"
            Args:
                x           : B, T - values in [-1, 1]

            Outputs:
                z           : B, Q (quantize_channel), L
                indice      :
        """
        h = self.fbank(x).transpose(1, 2) # B, C, L
        h = self.encoder(h) # B, C, L

        z, indice, _ = self.quantizer(h.transpose(1, 2)) # B, L, Q
        z = z.transpose(1, 2) # B, Q, L

        return z, indice

##############################################

class Decoder(nn.Module):

    def __init__(
            self, 
            in_channel, 
            jitter,
            num_spk,
            spk_channel,
            condition_channel,
            mulaw_size,
            mulaw_channel,  
            generate_channel,
            fc_channel):
        
        super(Decoder, self).__init__()

        self.generate_channel = generate_channel
        self.mulaw_size = mulaw_size

        self.jitter = Jitter(jitter)

        self.spk_emb = nn.Embedding(num_spk, spk_channel)

        self.condition_rnn = nn.GRU(
            in_channel + spk_channel, 
            condition_channel,
            num_layers=2, 
            batch_first=True, 
            bidirectional=True)

        self.mulaw_emb = nn.Embedding(self.mulaw_size, mulaw_channel)

        self.generate_rnn = nn.GRU(mulaw_channel + 2*condition_channel, generate_channel, batch_first=True)
        self.fc1 = Linear(generate_channel, fc_channel)
        self.fc2 = Linear(fc_channel, self.mulaw_size)

    def forward(self, z, prev_y, spk_id):
        """"
            Args:
                z       : B, C, L - encoder output
                prev_y  : B, T  -  previous signal encoded in mulaw
                spk_id  : B

            Outputs:
                y_hat   : B, T, C  - values in logits
        """
        z = self.jitter(z.transpose(1, 2)) # B, L, C

        z = F.interpolate(z.transpose(1, 2), scale_factor=2) # B, C, L (upsample)
        z = z.transpose(1, 2) # B, L, C

        spk = self.spk_emb(spk_id) # B, spk_channel
        spk = spk.unsqueeze(1).expand(-1, z.shape[1], -1) # B, L, spk_channel

        z = torch.cat((z, spk), dim=2) # B, L, (C + style_channel + spk_channel)
        z, _ = self.condition_rnn(z) # B, L, condition_channel

        z = F.interpolate(z.transpose(1, 2), scale_factor=160) # upsample to obtain signal: B, condition_dim, T (upsample)
        z = z.transpose(1, 2) # B, T, condition_dim
        z = z[:, :5120, :]

        prev_y = self.mulaw_emb(prev_y) # B, T, mulaw_channel

        o, _ = self.generate_rnn(torch.cat((prev_y, z), dim=2)) # B, T, rnn_channel
        
        y_hat = F.relu(self.fc1(o)) # B, T, fc_channel

        y_hat = self.fc2(y_hat) # B, T, mulaw_channel
       
        return y_hat

    def generate(self, z, spk_id):
        """"
            Args:
                z       : B, C, L - encoder output
                spk_id  : B

            Outputs:
                y_hat   : B, T, C  - values in probabilities
        """
        output = []

        z = F.interpolate(z, scale_factor=2) # B, C, L
        z = z.transpose(1, 2) # B, L, C

        spk = self.spk_emb(spk_id) # B, spk_channel
        spk = spk.unsqueeze(1).expand(-1, z.shape[1], -1) # B, L, spk_channel

        z = torch.cat((z, spk), dim=2) # B, L, (C + style_channel + spk_channel)
        z, _ = self.condition_rnn(z) # B, L, condition_channel

        z = F.interpolate(z.transpose(1, 2), scale_factor=160)
        z = z.transpose(1, 2)

        batch_size, _, _ = z.size()

        h = torch.zeros(batch_size, self.generate_channel, device=z.device)
        prev_y = torch.zeros(batch_size, device=z.device).fill_((self.mulaw_size - 1) // 2).long()

        cell = get_gru_cell(self.generate_rnn)
        
        for m in tqdm(torch.unbind(z, dim=1), leave=False):
            prev_y = self.mulaw_emb(prev_y)

            h = cell(torch.cat((prev_y, m), dim=1), h)

            y_hat = F.relu(self.fc1(h))

            logits = self.fc2(y_hat)

            dist = Categorical(logits=logits)
            y_hat = dist.sample()
    
            output.append(y_hat.item())

            prev_y = y_hat

        output = np.asarray(output)
        output = mulaw_decode(output, self.mulaw_size)
        return output

##############################################
    
class VQVAE(nn.Module):   

    def __init__(self, **config):
        super(VQVAE, self).__init__()
        
        self.encoder = Encoder(**config['encoder'])
        self.decoder = Decoder(**config['decoder'])

    def forward(self, x, prev_y, spk_id):
        """"
            Args:
                x           : B, T - values in [-1, 1]
                prev_y      : B, T - previous signal encoded in mulaw
                spk_id      : B, 1

            Outputs:
                y_hat       : B, T, C - values as probabilities
                vq_loss     : 
                perplexity  :
                active      :
        """
        z, vq_loss, perplexity, active = self.encoder(x)

        y_hat = self.decoder(z, prev_y, spk_id)

        return y_hat, vq_loss, perplexity, active

    def generate(self, x, spk_id):
        """"
            Args:
                z       : B, C, L - encoder output
                spk_id  : B

            Outputs:
                y_hat   : B, T, C  - values in [-1, 1]
        """
        z, _ = self.encoder.encode(x)

        x_hat = self.decoder.generate(z, spk_id)

        return x_hat
