import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Categorical
from tqdm import tqdm
from util import mulaw_decode


def get_gru_cell(gru):
    gru_cell = nn.GRUCell(gru.input_size, gru.hidden_size)
    gru_cell.weight_hh.data = gru.weight_hh_l0.data
    gru_cell.weight_ih.data = gru.weight_ih_l0.data
    gru_cell.bias_hh.data = gru.bias_hh_l0.data
    gru_cell.bias_ih.data = gru.bias_ih_l0.data
    return gru_cell


def get_silent(shape, mulaw_size=256):
    mulaw_size = mulaw_size - 1
    return torch.zeros(shape).fill_(mulaw_size // 2).long()


class VQEmbeddingEMA(nn.Module):

    def __init__(self, num_emb, emb_dim, commitment_cost=0.25, decay=0.99, epsilon=1e-5):
        super(VQEmbeddingEMA, self).__init__()
        
        self.commitment_cost = commitment_cost
        self.decay = decay
        self.epsilon = epsilon

        init_bound = 1 / 512
        embedding = torch.Tensor(num_emb, emb_dim)
        embedding.uniform_(-init_bound, init_bound)
        self.register_buffer("embedding", embedding)
        self.register_buffer("ema_count", torch.zeros(num_emb))
        self.register_buffer("ema_weight", self.embedding.clone())

    def encode(self, x):
        """"
            x: B, L, C
        """

        M, D = self.embedding.size()
        x_flat = x.detach().reshape(-1, D)

        distances = torch.addmm(torch.sum(self.embedding ** 2, dim=1) +
                                torch.sum(x_flat ** 2, dim=1, keepdim=True),
                                x_flat, self.embedding.t(),
                                alpha=-2.0, beta=1.0)

        indices = torch.argmin(distances.float(), dim=-1)
        quantized = F.embedding(indices, self.embedding)
        quantized = quantized.view_as(x)
        return quantized, indices

    def forward(self, x):
        M, D = self.embedding.size()
        x_flat = x.detach().reshape(-1, D)

        distances = torch.addmm(torch.sum(self.embedding ** 2, dim=1) +
                                torch.sum(x_flat ** 2, dim=1, keepdim=True),
                                x_flat, self.embedding.t(),
                                alpha=-2.0, beta=1.0)

        indices = torch.argmin(distances.float(), dim=-1)
        encodings = F.one_hot(indices, M).float()
        quantized = F.embedding(indices, self.embedding)
        quantized = quantized.view_as(x)

        if self.training:
            self.ema_count = self.decay * self.ema_count + (1 - self.decay) * torch.sum(encodings, dim=0)

            n = torch.sum(self.ema_count)
            self.ema_count = (self.ema_count + self.epsilon) / (n + M * self.epsilon) * n

            dw = torch.matmul(encodings.t(), x_flat)
            self.ema_weight = self.decay * self.ema_weight + (1 - self.decay) * dw

            self.embedding = self.ema_weight / self.ema_count.unsqueeze(-1)

        e_latent_loss = F.mse_loss(x, quantized.detach())
        loss = self.commitment_cost * e_latent_loss

        quantized = x + (quantized - x).detach()

        avg_probs = torch.mean(encodings, dim=0)
        perplexity = torch.exp(-torch.sum(avg_probs * torch.log(avg_probs + 1e-10)))

        return quantized, loss, perplexity


class Jitter(nn.Module):

    def __init__(self, p):
        super(Jitter, self).__init__()
        
        self.p = p
        prob = torch.Tensor([p / 2, 1 - p, p / 2])
        self.register_buffer("prob", prob)

    def forward(self, x):
        """"
            x   : B, L, C
        """
        if not self.training or self.p == 0:
            return x
        else:
            batch_size, sample_size, channels = x.size()

            dist = Categorical(self.prob)
            index = dist.sample(torch.Size([batch_size, sample_size])) - 1
            index[:, 0].clamp_(0, 1)
            index[:, -1].clamp_(-1, 0)
            index += torch.arange(sample_size, device=x.device)

            x = torch.gather(x, 1, index.unsqueeze(-1).expand(-1, -1, channels))
        return x
    
    
class Encoder(nn.Module):

    def __init__(self, in_channel, hidden_channel, quantize_size, quantize_channel, commitment_cost=0.25, decay=0.99, epsilon=1e-5):
        super(Encoder, self).__init__()

        self.encoder = nn.Sequential(
            nn.Conv1d(in_channel, hidden_channel, 3, 1, 1, bias=False),
            nn.BatchNorm1d(hidden_channel),
            nn.ReLU(True),
            nn.Conv1d(hidden_channel, hidden_channel, 3, 1, 1, bias=False),
            nn.BatchNorm1d(hidden_channel),
            nn.ReLU(True),
            nn.Conv1d(hidden_channel, hidden_channel, 4, 2, 2, bias=False), # downsample by 2
            nn.BatchNorm1d(hidden_channel),
            nn.ReLU(True),
            nn.Conv1d(hidden_channel, hidden_channel, 3, 1, 1, bias=False),
            nn.BatchNorm1d(hidden_channel),
            nn.ReLU(True),
            nn.Conv1d(hidden_channel, hidden_channel, 3, 1, 1, bias=False),
            nn.BatchNorm1d(hidden_channel),
            nn.ReLU(True),
            nn.Conv1d(hidden_channel, quantize_channel, 1, 1) # project output channel to embedding dimension
        )

        self.codebook = VQEmbeddingEMA(quantize_size, quantize_channel, commitment_cost, decay, epsilon)
      
    def forward(self, x):
        """"
            Args:
                x           : B, C, L - feature

            Outputs:
                z           : B, E (quantize), L (downsample)
                vq_loss
                perplexity
        """
        z = self.encoder(x) # B, E, L (downsample)

        z, vq_loss, perplexity = self.codebook(z.transpose(1, 2)) # B, L, E
        z = z.transpose(1, 2)

        return z, vq_loss, perplexity

    def encode(self, x):
        """"
            Args:
                x       : B, C, L - feature

            Outputs:
                z       : B, E (quantize), L (downsample)
        """
        z = self.encoder(x)

        z, indices = self.codebook.encode(z.transpose(1, 2))
        z = z.transpose(1, 2)
    
        return z, indices


class Decoder(nn.Module):

    def __init__(
            self, 
            in_channel, 
            jitter,
            num_spk, 
            spk_channel,
            condition_channel,
            mulaw_size,
            mulaw_channel,  
            generate_channel,
            fc_channel):
        
        super(Decoder, self).__init__()

        self.generate_channel = generate_channel
        self.mulaw_size = mulaw_size

        self.jitter = Jitter(jitter)
        
        self.spk_emb = nn.Embedding(num_spk, spk_channel)

        self.condition_rnn = nn.GRU(
            in_channel + spk_channel, 
            condition_channel,
            num_layers=2, 
            batch_first=True, 
            bidirectional=True)

        self.mulaw_emb = nn.Embedding(self.mulaw_size, mulaw_channel)

        self.generate_rnn = nn.GRU(mulaw_channel + 2*condition_channel, generate_channel, batch_first=True)
        self.fc1 = nn.Linear(generate_channel, fc_channel)
        self.fc2 = nn.Linear(fc_channel, self.mulaw_size)

    def forward(self, z, prev_y, spk_id):
        """"
            Args:
                z       : B, C, L - encoder output
                prev_y  : B, T  -  previous signal encoded in mulaw
                spk_id  : B

            Outputs:
                y_hat   : B, T, C  - values in logits
        """
        z = self.jitter(z.transpose(1, 2)) # B, L, C

        z = F.interpolate(z.transpose(1, 2), scale_factor=2) # B, C, L (upsample)
        z = z.transpose(1, 2) # B, L, C

        spk_emb = self.spk_emb(spk_id) # B, spk_channel
        spk_emb = spk_emb.unsqueeze(1).expand(-1, z.shape[1], -1) # B, L, spk_channel
        
        z = torch.cat((z, spk_emb), dim=2) # B, L, (C + spk_channel)
        z, _ = self.condition_rnn(z) # B, L, condition_channel

        z = F.interpolate(z.transpose(1, 2), scale_factor=160) # upsample to obtain signal: B, condition_dim, T (upsample)
        z = z.transpose(1, 2) # B, T, condition_dim

        prev_y = self.mulaw_emb(prev_y) # B, T, mulaw_channel

        y_hat, _ = self.generate_rnn(torch.cat((prev_y, z), dim=2)) # B, T, rnn_channel
        
        y_hat = F.relu(self.fc1(y_hat)) # B, T, fc_channel

        y_hat = self.fc2(y_hat) # B, T, mulaw_channel
       
        return y_hat

    def generate(self, z, spk_id):
        """"
            Args:
                z       : B, C, L - encoder output
                prev_y  : B, T  -  previous signal encoded in mulaw
                spk_id  : B

            Outputs:
                y_hat   : B, T  - values in [-1, 1]
        """
        output = []  
        cell = get_gru_cell(self.generate_rnn)

        z = F.interpolate(z, scale_factor=2)
        z = z.transpose(1, 2)

        spk_emb = self.spk_emb(spk_id)
        spk_emb = spk_emb.unsqueeze(1).expand(-1, z.size(1), -1)

        z = torch.cat((z, spk_emb), dim=2)
        z, _ = self.condition_rnn(z)

        z = F.interpolate(z.transpose(1, 2), scale_factor=160)
        z = z.transpose(1, 2)

        batch_size, _, _ = z.size()

        h = torch.zeros(batch_size, self.generate_channel, device=z.device)
        prev_y = torch.zeros(batch_size, device=z.device).fill_((self.mulaw_size - 1) // 2).long()

        for m in tqdm(torch.unbind(z, dim=1), leave=False):
            prev_y = self.mulaw_emb(prev_y)

            h = cell(torch.cat((prev_y, m), dim=1), h)

            y_hat = F.relu(self.fc1(h))

            logits = self.fc2(y_hat)

            dist = Categorical(logits=logits)
            y_hat = dist.sample()
    
            output.append(y_hat.item())

            prev_y = y_hat

        output = np.asarray(output)
        output = mulaw_decode(output, self.mulaw_size)
        return output


class VQVAE(nn.Module):   

    def __init__(self, **config):
        super(VQVAE, self).__init__()

        self.encoder = Encoder(**config['encoder'])
        self.decoder = Decoder(**config['decoder'])

    def forward(self, x, prev_y, spk_id):
        """"
            Args:
                x       : B, C, L
                prev_y  : B, T - previous signal encoded in mulaw
                spk_id  : B

            Outputs:
                y_hat       : B, T, C - values in probabilities
                vq_loss
                perplexity
        """
        x = x.float()
        prev_y = prev_y.long()
        spk_id = spk_id.long()
    
        z, vq_loss, perplexity = self.encoder(x)

        y_hat = self.decoder(z, prev_y, spk_id)

        return y_hat, vq_loss, perplexity

    def generate(self, x, spk_id):
        """"
            Args:
                z       : B, C, L - encoder output
                spk_id  : B

            Outputs:
                y_hat   : B, T, C  - values in probabilities
        """
        x = x.float()
        spk_id = spk_id.long()

        z, _ = self.encoder.encode(x)

        x_hat = self.decoder.generate(z, spk_id)

        return x_hat
