import torch
import torch.nn as nn
import torch.nn.functional as F
from vector_quantize_pytorch import VectorQuantize
from util import get
from module.jitter import Jitter
from wavenet.wavenet import WaveNet


class Conv1d(nn.Module):

    def __init__(self, 
                 in_channel, 
                 out_channel, 
                 kernel, 
                 stride=1, 
                 padding=0, 
                 bias=True,
                 bath_norm=False, 
                 residual=False,
                 act_func=F.relu):
        super(Conv1d, self).__init__()

        self.residual = residual
        self.act_func = act_func

        conv = nn.Conv1d(
            in_channels=in_channel,
            out_channels=out_channel,
            kernel_size=kernel,
            stride=stride,
            padding=padding,
            bias=bias
        )

        self.pipeline = nn.Sequential(conv)
    
        if bath_norm:
            self.pipeline.append(nn.BatchNorm1d(out_channel))

    def forward(self, x):
        if self.act_func is not None and callable(self.act_func):
            y = self.act_func(self.pipeline(x))
        else:
            y = self.pipeline(x)

        if self.residual:
            y = y + x

        return y


class Encoder(nn.Module):

    def __init__(self, **config):
        super(Encoder, self).__init__()
        
        in_channel = get(config, 'in_channel', 39)
        hidden_channel = get(config, 'hidden_channel', 768)

        #######
        quantize_size = get(config, 'quantize_size')
        quantize_channel = get(config, 'quantize_channel', 64)
        commitment_cost = get(config, 'commitment_cost', 0.25)
        decay = get(config, 'decay', 0.99)
        epsilon = get(config, 'epsilon', 1e-5)

        #######
        self.encoder = nn.Sequential(
            Conv1d(in_channel, hidden_channel, 3, 1, 1),
            Conv1d(hidden_channel, hidden_channel, 3, 1, 1, residual=True),
            Conv1d(hidden_channel, hidden_channel, 4, 2, 2),
            Conv1d(hidden_channel, hidden_channel, 3, 1, 1, residual=True),
            Conv1d(hidden_channel, hidden_channel, 3, 1, 1, residual=True),

            Conv1d(hidden_channel, hidden_channel, 1, 1, residual=True),
            Conv1d(hidden_channel, hidden_channel, 1, 1, residual=True),
            Conv1d(hidden_channel, hidden_channel, 1, 1, residual=True),
            Conv1d(hidden_channel, hidden_channel, 1, 1, residual=True),

            Conv1d(hidden_channel, quantize_channel, 1, 1), # project to num_emb
        )

        self.quantizer = VectorQuantize(
            dim=quantize_channel,
            codebook_size=quantize_size,     
            codebook_dim=quantize_channel,
            commitment_weight=commitment_cost,
            decay=decay,             
            eps=epsilon
        )

    def forward(self, x):
        """"
            x: B, C, T
            z: B, E, L
        """
        z = self.encoder(x.to(dtype=torch.float32)) # B, C, L
    
        z, vq_loss, perplexity = self.quantizer(z.transpose(1, 2)) # B, L, E
        z = z.transpose(1, 2) # B, E, L

        return z, vq_loss, perplexity


class Decoder(nn.Module):   

    def __init__(self, **config):
        super(Decoder, self).__init__()

        jitter = get(config, 'jitter', 0.12)
        in_channel = get(config, 'in_channel', 768)
        conv_channel = get(config, 'conv_channel', 128)

        if jitter:
            self.jitter = Jitter(jitter)

        self.conv1 = Conv1d(in_channel, conv_channel, 3, 1, 1)

        self.upsample = nn.Upsample(scale_factor=320) # upsample from 50Hz to 16KHz

        self.wavenet = WaveNet(
            in_channels=conv_channel,
            out_channels=config['out_channel'],
            layers=config['num_layer'],
            stacks=config['num_stack'],
            residual_channels=config['residual_channel'],
            gate_channels=config['gate_channel'],
            skip_out_channels=config['skip_out_channel'],
            cin_channels=config['local_condition_channel'],
            gin_channels=config['global_condition_channel'],
            use_speaker_embedding=config['use_speaker_embedding'],
            n_speakers=config['num_spk'],
            scalar_input=True,
        )
    
    def forward(self, z, prev_y, spk_id):
        """"
            Args:
                z       : B, C, T
                prev_y  : B, T - [-1, 1]
                spk_id  : B, 1

            Outputs:
                y_hat   : B, C, T in logits
        """
        if self.jitter is not None and self.training:
            z = self.jitter(z)

        z = self.conv1(z)

        z = self.upsample(z)

        y_hat = self.wavenet(x=prev_y.unsqueeze(1), c=z, g=spk_id, softmax=True) # B, C, L

        return y_hat

    def generate(self, z, spk_id):
        z = self.conv1(z)

        z = self.upsample(z)

        x_hat = self.wavenet.incremental_forward(c=z, g=spk_id)
     
        return x_hat


class VQVAE(nn.Module):   

    def __init__(self, **config):
        super(VQVAE, self).__init__()

        self.encoder = Encoder(**config['encoder'])
        self.decoder = Decoder(**config['decoder'])

    def forward(self, x, prev_y, spk_id):
        """"
            Args:
                x       : B, C, T
                sig     : B, T - [-1, 1]
                spk_id  : B, 1

            Outputs:
                x_hat   : B, T
        """
        x = x.float()
        prev_y = prev_y.float()
        spk_id = spk_id.float()
    
        z, vq_loss, perplexity = self.encoder(x)

        x_hat = self.decoder(z, prev_y, spk_id)

        return x_hat, vq_loss, perplexity
