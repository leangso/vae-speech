import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchaudio.functional as AF
from module import Conv, Linear, get_gru_cell
from module.filter import GaborConv, SincConv, GammatoneConv, GaussianConv
from module.jitter import Jitter
from torch.distributions import Categorical
from tqdm import tqdm
from util import get, mulaw_decode
from vector_quantize_pytorch import VectorQuantize


class Encoder(nn.Module):

    def __init__(self, **config):
        super(Encoder, self).__init__()

        ####### filter
        filt_type = get(config, 'filt_type', 'gabor') # sinc
        num_filt = get(config, 'num_filt', 40)
        sample_rate = get(config, 'sample_rate', 16000)
        win_len = get(config, 'win_len', 401)
        win_step = get(config, 'win_step', 160)
        num_fft = get(config, 'num_fft', 512)
        min_freq = get(config, 'min_freq', 50)
        pre_emph = get(config, 'pre_emph', None)
        use_pcen = get(config, 'use_pcen', False)
        learnable_pcen = get(config, 'learnable_pcen', False)

        #######
        hidden_channel = get(config, 'hidden_channel', 768)
        prosody_channel = get(config, 'prosody_channel', 64)

        quantize_size = get(config, 'quantize_size')
        quantize_channel = get(config, 'quantize_channel', 64)
        commitment_cost = get(config, 'commitment_cost', 0.25)
        decay = get(config, 'decay', 0.99)
        epsilon = get(config, 'epsilon', 1e-5)

        #######

        if filt_type == 'gabor':
            self.conv = GaborConv(
                in_channels=1,
                out_channels=num_filt,
                kernel_size=win_len,
                stride=win_step,
                padding='valid',
                n_fft=num_fft,
                sample_rate=sample_rate,
                min_freq=min_freq,
                use_pcen=use_pcen,
                learnable_pcen=learnable_pcen
            )
        elif filt_type == 'sinc':
            self.conv = SincConv(
                in_channels=1,
                out_channels=num_filt,
                kernel_size=win_len,
                stride=win_step,
                padding='valid',
                sample_rate=sample_rate,
                min_low_hz=min_freq,
            )
        elif filt_type == 'gammatone':
            self.conv = GammatoneConv(
                in_channels=1,
                out_channels=num_filt,
                kernel_size=win_len,
                stride=win_step,
                padding='valid',
                sample_rate=sample_rate,
                min_freq=min_freq,
                order=4
            )
        elif filt_type == 'gaussian':
            self.conv = GaussianConv(
                in_channels=1,
                out_channels=num_filt,
                kernel_size=win_len,
                stride=win_step,
                padding='valid',
                sample_rate=sample_rate,
                min_freq=min_freq
            )
        else:
            raise Exception('filter type is unknown: ', filt_type)
        
        self.encoder = nn.Sequential(
            Conv(num_filt, hidden_channel, 3, 1, 1),
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),

            Conv(hidden_channel, hidden_channel, 3, 1, 1),
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),
            
            Conv(hidden_channel, hidden_channel, 4, 2, 2), # downsample by 2
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),

            Conv(hidden_channel, hidden_channel, 3, 1, 1),
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),

            Conv(hidden_channel, hidden_channel, 3, 1, 1),
            nn.ReLU(True),
            nn.BatchNorm1d(hidden_channel),

            Conv(hidden_channel, quantize_channel, 1, 1) # project output channel to embedding dimension
        )

        self.linear1 = Linear(2 * quantize_channel, prosody_channel)
        self.linear2 = Linear(prosody_channel, prosody_channel)

        self.quantizer = VectorQuantize(
            dim=quantize_channel,
            codebook_size=quantize_size,     
            codebook_dim=quantize_channel,
            commitment_weight=commitment_cost,
            decay=decay,             
            eps=epsilon
        )

        self.filt_type = filt_type
        self.pre_emph = pre_emph
        self.quantize_size = quantize_size

    def forward(self, x):
        """"
            Args:
                x           : B, T - values in [-1, 1]

            Outputs:
                z           : B, Q (quantize_channel), L
                p           : B, prosody_channel
                vq_loss     :
                perplexity  :
        """
        h = self._preprocess(x) # B, C, L
        h = self.encoder(h) # B, C, L

        # content information
        z, indice, vq_loss = self.quantizer(h.transpose(1, 2)) # B, L, Q
        z = z.transpose(1, 2) # B, Q, L

        # prosody information
        p = h - z
        std, mean = torch.std_mean(p, dim=2)
        p = torch.cat((mean, std), dim=-1)
        p = self.linear1(p)
        p = self.linear2(p)

        # perplexity & active code
        encoding = F.one_hot(indice.reshape(-1), self.quantize_size).float()
        avg_prob = torch.mean(encoding, dim=0)
        perplexity = torch.exp(-torch.sum(avg_prob * torch.log(avg_prob + 1e-10)))

        active = indice.unique().numel() / self.quantize_size * 100
        
        return z, p, vq_loss, perplexity, active

    def encode(self, x):
        """"
            Args:
                x           : B, T - values in [-1, 1]

            Outputs:
                z           : B, Q (quantize_channel), L
                indice      :
        """
        h = self._preprocess(x) # B, C, L
        h = self.encoder(h) # B, C, L

        # content information
        z, indice, _ = self.quantizer(h.transpose(1, 2)) # B, L, Q
        z = z.transpose(1, 2) # B, Q, L

        # prosody information
        p = h - z
        std, mean = torch.std_mean(p, dim=2)
        p = torch.cat((mean, std), dim=-1)
        p = self.linear1(p)
        p = self.linear2(p)

        return z, p, indice

    def _preprocess(self, x):
        """"
            Args:
                x           : B, T - values in [-1, 1]

            Outputs:
                h           : B, C, L
        """
        if self.pre_emph:
            x = AF.preemphasis(x, self.pre_emph)

        h = self.conv(x)
        if self.filt_type == 'sinc':
            h = h.transpose(1, 2)

        return h

##############################################

class Decoder(nn.Module):

    def __init__(
            self, 
            in_channel, 
            jitter,
            num_spk,
            spk_channel,
            prosody_channel,
            condition_channel,
            mulaw_size,
            mulaw_channel,  
            generate_channel,
            fc_channel):
        
        super(Decoder, self).__init__()

        self.generate_channel = generate_channel
        self.mulaw_size = mulaw_size

        self.jitter = Jitter(jitter)

        self.spk_emb = nn.Embedding(num_spk, spk_channel)

        self.condition_rnn = nn.GRU(
            in_channel + prosody_channel + spk_channel, 
            condition_channel,
            num_layers=2, 
            batch_first=True, 
            bidirectional=True)

        self.mulaw_emb = nn.Embedding(self.mulaw_size, mulaw_channel)

        self.generate_rnn = nn.GRU(mulaw_channel + 2*condition_channel, generate_channel, batch_first=True)
        self.fc1 = Linear(generate_channel, fc_channel)
        self.fc2 = Linear(fc_channel, self.mulaw_size)

    def forward(self, z, p, prev_y, spk_id):
        """"
            Args:
                z       : B, C, L - encoder output
                p       : B, C
                prev_y  : B, T  -  previous signal encoded in mulaw
                spk_id  : B

            Outputs:
                y_hat   : B, T, C  - values in logits
        """
        z = self.jitter(z.transpose(1, 2)) # B, L, C

        z = F.interpolate(z.transpose(1, 2), scale_factor=2) # B, C, L (upsample)
        z = z.transpose(1, 2) # B, L, C

        p = p.unsqueeze(1).expand(-1, z.shape[1], -1) # B, L, style_channel

        spk = self.spk_emb(spk_id) # B, spk_channel
        spk = spk.unsqueeze(1).expand(-1, z.shape[1], -1) # B, L, spk_channel

        z = torch.cat((z, p, spk), dim=2) # B, L, (C + style_channel + spk_channel)
        z, _ = self.condition_rnn(z) # B, L, condition_channel

        z = F.interpolate(z.transpose(1, 2), scale_factor=160) # upsample to obtain signal: B, condition_dim, T (upsample)
        z = z.transpose(1, 2) # B, T, condition_dim

        prev_y = self.mulaw_emb(prev_y) # B, T, mulaw_channel

        o, _ = self.generate_rnn(torch.cat((prev_y, z), dim=2)) # B, T, rnn_channel
        
        y_hat = F.relu(self.fc1(o)) # B, T, fc_channel

        y_hat = self.fc2(y_hat) # B, T, mulaw_channel
       
        return y_hat

    def generate(self, z, p, spk_id):
        """"
            Args:
                z       : B, C, L - encoder output
                p       : B, C
                spk_id  : B

            Outputs:
                y_hat   : B, T, C  - values in probabilities
        """
        output = []  

        z = F.interpolate(z, scale_factor=2) # B, C, L
        z = z.transpose(1, 2) # B, L, C
        
        p = p.unsqueeze(1).expand(-1, z.shape[1], -1) # B, L, style_channel

        spk = self.spk_emb(spk_id) # B, spk_channel
        spk = spk.unsqueeze(1).expand(-1, z.shape[1], -1) # B, L, spk_channel

        z = torch.cat((z, p, spk), dim=2) # B, L, (C + style_channel + spk_channel)
        z, _ = self.condition_rnn(z) # B, L, condition_channel

        z = F.interpolate(z.transpose(1, 2), scale_factor=160)
        z = z.transpose(1, 2)

        batch_size, _, _ = z.size()

        h = torch.zeros(batch_size, self.generate_channel, device=z.device)
        prev_y = torch.zeros(batch_size, device=z.device).fill_((self.mulaw_size - 1) // 2).long()

        cell = get_gru_cell(self.generate_rnn)
        
        for m in tqdm(torch.unbind(z, dim=1), leave=False):
            prev_y = self.mulaw_emb(prev_y)

            h = cell(torch.cat((prev_y, m), dim=1), h)

            y_hat = F.relu(self.fc1(h))

            logits = self.fc2(y_hat)

            dist = Categorical(logits=logits)
            y_hat = dist.sample()
    
            output.append(y_hat.item())

            prev_y = y_hat

        output = np.asarray(output)
        output = mulaw_decode(output, self.mulaw_size)
        return output

##############################################
    
class VQVAE(nn.Module):   

    def __init__(self, **config):
        super(VQVAE, self).__init__()

        self.encoder = Encoder(**config['encoder'])
        self.decoder = Decoder(**config['decoder'])

    def forward(self, x, prev_y, spk_id):
        """"
            Args:
                x           : B, T - values in [-1, 1]
                prev_y      : B, T - previous signal encoded in mulaw
                spk_id      : B, 1

            Outputs:
                y_hat       : B, T, C - values as probabilities
                vq_loss     :
                perplexity  :
        """
        z, p, vq_loss, perplexity, active = self.encoder(x)

        y_hat = self.decoder(z, p, prev_y, spk_id)

        return y_hat, vq_loss, perplexity, active

    def generate(self, x, spk_id):
        """"
            Args:
                z       : B, C, L - encoder output
                spk_id  : B

            Outputs:
                y_hat   : B, T, C  - values in [-1, 1]
        """
        z, p, _ = self.encoder.encode(x)

        x_hat = self.decoder.generate(z, p, spk_id)

        return x_hat
