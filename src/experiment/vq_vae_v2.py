import torch.nn as nn
import torch.optim as optim
from experiment.experiment import Experiment
from model.vq_vae_v2 import VQVAE
from util.file import read_yaml_file
from util import mulaw_encode


class VQVAE_EXP(Experiment): 

    def __init__(self, **config):
        super().__init__(**config) 

        model_config = read_yaml_file(self.config['model_file'])

        self.model = VQVAE(**model_config)
        
        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.Adam(self.parameters(), lr=self.config['learning_rate'], amsgrad=True)

    def training_step(self, batch, batch_idx):
        spk_id, x = batch[0].long(), batch[2].float()

        x_mulaw = mulaw_encode(x)
        prev_y = x_mulaw[:, :-1]

        # forward
        y_hat, vq_loss, perplexity, active = self.forward(x[:, 1:], prev_y.long(), spk_id)

        recon_loss = self.criterion(y_hat.transpose(1, 2).float(), x_mulaw[:, 1:].long())
        loss = vq_loss + recon_loss

        self.log_dict({
            'loss': loss,
            'recon_loss': recon_loss,
            'vq_loss': vq_loss,
            'perplexity': perplexity,
            'active': active
        }, batch_size=x.shape[0], on_step=True, on_epoch=True, prog_bar=True, logger=True)

        return loss

    def test_step(self, batch, batch_idx, dataloader_idx):
        return self.training_step(batch, batch_idx)
