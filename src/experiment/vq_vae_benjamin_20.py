import torch
import torch.nn as nn
import torch.optim as optim
from experiment.experiment import Experiment
from model.vq_vae_benjamin_20 import VQVAE, get_silent
from util.file import read_yaml_file
from util import mulaw_encode


class VQVAE_EXP(Experiment): 

    def __init__(self, **config):
        super().__init__(**config) 

        model_config = read_yaml_file(self.config['model_file'])

        self.model = VQVAE(**model_config)
        
        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.Adam(self.parameters(), lr=self.config['learning_rate'], amsgrad=True)

    def training_step(self, batch, batch_idx):
        spk_id, x, sig_mulaw = batch[0].long(), batch[1].float(), batch[2].long()

        silent = get_silent((sig_mulaw.shape[0], 1)).to(sig_mulaw.device)
        prev_y = torch.cat((silent, sig_mulaw[:, :-1]), dim=1)

        # forward
        y_hat, vq_loss, perplexity = self.forward(x.transpose(1, 2), prev_y, spk_id)

        recon_loss = self.criterion(y_hat.transpose(1, 2).float(), sig_mulaw)
        loss = vq_loss + recon_loss

        self.log_dict({
            'loss': loss,
            'recon_loss': recon_loss,
            'vq_loss': vq_loss,
            'perplexity': perplexity
        }, batch_size=x.shape[0], on_step=True, on_epoch=True, prog_bar=True, logger=True)

        return loss

    def test_step(self, batch, batch_idx, dataloader_idx):
        return self.training_step(batch, batch_idx)
