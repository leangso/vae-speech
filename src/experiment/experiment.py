import copy
import logging
import os
import shutil
from logging import handlers
from os import path

from lightning.pytorch import LightningDataModule, LightningModule, Trainer
from lightning.pytorch.callbacks import ModelCheckpoint, TQDMProgressBar
from lightning.pytorch.callbacks.early_stopping import EarlyStopping
from lightning.pytorch.loggers import TensorBoardLogger
from util.file import read_yaml_file, save_yaml_file
from util.logger import (TruncateNameFilter, attach_logger, detach_logger, format, get_logger)


def _merge_config(config1: dict, config2: dict):
    new_config = copy.deepcopy(config1)
    for key in config2:
        if key in ['experiments'] or key in new_config.keys():
            continue
        new_config[key] = config2[key]

    return new_config


def load_exp_configs(conf_file: str) -> dict:
    exp_configs = list()
    
    main_config = read_yaml_file(conf_file)
    for exp_config in main_config['experiments']:
        new_conf = _merge_config(exp_config, main_config)
        exp_configs.append(new_conf)
    
    return exp_configs


class DataSource(LightningDataModule):

    def __init__(self):
        super().__init__()


class Experiment(LightningModule):

    def __init__(self, **config):
        super().__init__()

        self.config = config
        self.name = config['name']
        self.skip = config.get('skip')
        if self.skip is None:
            self.skip = False

        # output dir
        self.output_dir = config['output_dir']
        self.exp_dir = path.join(self.output_dir, '%s' % self.name)
        os.makedirs(self.output_dir, exist_ok=True)
        os.makedirs(self.exp_dir, exist_ok=True)
        
        # init logger
        self.LOGGER = self.init_logger()

        if config.get('clean') is True:
            self.LOGGER.debug(f'Clean experiment directory: {self.exp_dir}')
            shutil.rmtree(self.exp_dir, ignore_errors=True)
            os.makedirs(self.exp_dir, exist_ok=True)

        # write config files
        config_dir = path.join(self.exp_dir, 'config')
        os.makedirs(config_dir, exist_ok=True)
        
        exp_file = path.join(config_dir, 'exp.yaml')
        save_yaml_file(config, exp_file, default_flow_style=False, indent=4, sort_keys=False)
        shutil.copy(config['feat_file'], path.join(config_dir, 'feat.yaml'))
        shutil.copy(config['model_file'], path.join(config_dir, 'model.yaml'))
        
        # read feature config
        self.feat_config = read_yaml_file(config['feat_file'])

        # checkpoint dir
        self.chkpt_dir = path.join(self.exp_dir, 'checkpoint')
        os.makedirs(self.chkpt_dir, exist_ok=True)

        # init modules
        self.data_source = self.init_data_source()
        self.model = self.init_model()
        self.trainer = self.init_trainer()

    def init_logger(self):
        log_file = os.path.join(self.exp_dir, 'log.txt')
    
        file_handler = handlers.RotatingFileHandler(log_file, maxBytes=10485760, backupCount=7)
        file_handler.setFormatter(format)
        file_handler.addFilter(TruncateNameFilter(max_length=20))
        
        logger = get_logger(self.config['name'])
        logger.propagate = False 
        logger.setLevel(logging.DEBUG)
        logger.addHandler(file_handler)

        attach_logger(logger)

        return logger

    def init_model(self):
        return None
    
    def init_data_source(self):
        return None
    
    def init_trainer(self): 
        default_root_dir = self.exp_dir
        devices = self.conf('devices')
        accelerator = 'gpu' if self.conf('cuda') is True else 'cpu'

        log_every_n_step = self.conf('log_every_n_step', 50)
        max_epoch = self.conf('num_epoch')
        max_step = self.conf('num_step', -1)
        save_last = self.conf('save_last')
        save_top_k = self.conf('save_top_k', -1)
        monitor = self.conf('monitor', 'loss')

        gradient_clip_val = self.conf('gradient_clip_val')

        logger = TensorBoardLogger(
            save_dir=self.exp_dir,
            name='tboard',
            version=None,
            default_hp_metric=False,
        )

        # callbacks
        model_ckpt = ModelCheckpoint(
            dirpath=self.chkpt_dir, 
            monitor=monitor,
            save_last=save_last,
            save_top_k=save_top_k,
            every_n_epochs=self.conf('save_every_n_epoch'),
            every_n_train_steps=self.conf('save_every_n_step'),
        )

        callbacks = []
        callbacks.append(TQDMProgressBar(refresh_rate=log_every_n_step))
        callbacks.append(model_ckpt)

        if self.conf('early_stopping') is True:
            early_stopping = EarlyStopping(monitor=monitor, mode="min")
            callbacks.append(early_stopping)
        
        trainer = Trainer(
            logger=logger,
            default_root_dir=default_root_dir,
            devices=devices,
            accelerator=accelerator,
            callbacks=callbacks,
            log_every_n_steps=log_every_n_step,
            max_epochs=max_epoch,
            max_steps=max_step,
            gradient_clip_val=gradient_clip_val
        )
    
        return trainer

    def conf(self, param, default=None):
        value = self.config.get(param)
        if value is None:
            value = default
        return value

    def configure_optimizers(self):
        if hasattr(self, 'optimizer'):
            return self.optimizer
        
        raise NotImplementedError
    
    def forward(self, *args, **kwargs):
        if hasattr(self, 'optimizer'):
            return self.model(*args, **kwargs)
        else:
            return super().forward()
    
    def training_step(self, batch, batch_idx, *args, **kwargs):
        super().training_step(batch, batch_idx, *args, **kwargs)

    def test_step(self, batch, batch_idx, *args, **kwargs):
        super().test_step(batch, batch_idx, *args, **kwargs)
  
    def fit(self, data_loader=None, data_source=None, ckpt_file=None):
        if ckpt_file is None:
            ckpt_file = self.conf('ckpt_file')

        self.trainer.fit(model=self, train_dataloaders=data_loader, datamodule=data_source, ckpt_path=ckpt_file)

    def test(self, data_loader=None, data_source=None, ckpt_file=None):
        if ckpt_file is None:
            ckpt_file = self.conf('ckpt_file')
    
        self.trainer.test(model=self, dataloaders=data_loader, datamodule=data_source, ckpt_path=ckpt_file)

    #### event ####

    def on_train_start(self):
        self.LOGGER.debug(f'>>> Start training: {self.name}')

    def on_train_end(self):
        self.LOGGER.debug(f'>>> End training: {self.name}')

    def on_test_start(self) -> None:
        self.LOGGER.debug(f'>>> Start testing: {self.name}')

    def on_test_end(self) -> None:
        self.LOGGER.debug(f'>>> End training: {self.name}')

    def shutdown(self):
        detach_logger(self.LOGGER)

