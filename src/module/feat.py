import logging
import math

import numpy as np
import torch
import torch.nn as nn
from spafe.fbanks.gammatone_fbanks import (compute_gain, generate_center_frequencies)
from speechbrain.processing.features import (DCT, STFT, ContextWindow, Deltas, spectral_magnitude)

logger = logging.getLogger(__name__)


class Fbank(nn.Module):
    
    def __init__(
        self,
        deltas=False,
        context=False,
        requires_grad=False,
        sample_rate=16000,
        f_min=0,
        f_max=None,
        fc_ranges=None,
        n_fft=512,
        n_mels=40,
        filter_shape="triangular",
        param_change_factor=1.0,
        param_rand_factor=0.0,
        left_frames=5,
        right_frames=5,
        win_length=25,
        hop_length=10,
    ):
        super().__init__()
        self.deltas = deltas
        self.context = context
        self.requires_grad = requires_grad
        self.fc_ranges = fc_ranges

        if f_max is None:
            f_max = sample_rate / 2

        self.compute_STFT = STFT(
            sample_rate=sample_rate,
            n_fft=n_fft,
            win_length=win_length,
            hop_length=hop_length,
            center=True
        )
        self.compute_fbanks = Filterbank(
            sample_rate=sample_rate,
            n_fft=n_fft,
            n_mels=n_mels,
            f_min=f_min,
            f_max=f_max,
            fc_ranges=fc_ranges,
            freeze=not requires_grad,
            filter_shape=filter_shape,
            param_change_factor=param_change_factor,
            param_rand_factor=param_rand_factor,
        )
        self.compute_deltas = Deltas(input_size=n_mels)
        self.context_window = ContextWindow(
            left_frames=left_frames, right_frames=right_frames,
        )

    def forward(self, wav):
        """Returns a set of features generated from the input waveforms.

        Arguments
        ---------
        wav : tensor
            A batch of audio signals to transform to features.
        """
        STFT = self.compute_STFT(wav)
        mag = spectral_magnitude(STFT)
        fbanks = self.compute_fbanks(mag)

        if self.deltas:
            delta1 = self.compute_deltas(fbanks)
            delta2 = self.compute_deltas(delta1)
            fbanks = torch.cat([fbanks, delta1, delta2], dim=2)

        if self.context:
            fbanks = self.context_window(fbanks)

        return fbanks


class Filterbank(nn.Module):
    
    def __init__(
        self,
        n_mels=40,
        log_mel=True,
        filter_shape="triangular",
        f_min=0,
        f_max=8000,
        fc_ranges=None,
        n_fft=512,
        sample_rate=16000,
        power_spectrogram=2,
        amin=1e-10,
        ref_value=1.0,
        top_db=80.0,
        param_change_factor=1.0,
        param_rand_factor=0.0,
        smooth_factor=2, # only used for gaussian filter
        freeze=True,
    ):
        super().__init__()
        self.n_mels = n_mels
        self.log_mel = log_mel
        self.filter_shape = filter_shape
        self.f_min = f_min
        self.f_max = f_max
        self.n_fft = n_fft
        self.sample_rate = sample_rate
        self.power_spectrogram = power_spectrogram
        self.amin = amin
        self.ref_value = ref_value
        self.top_db = top_db
        self.freeze = freeze
        self.n_stft = self.n_fft // 2 + 1
        self.db_multiplier = math.log10(max(self.amin, self.ref_value))
        self.device_inp = torch.device("cpu")
        self.param_change_factor = param_change_factor
        self.param_rand_factor = param_rand_factor
        self.smooth_factor = smooth_factor
        self.fc_ranges = fc_ranges

        if self.power_spectrogram == 2:
            self.multiplier = 10
        else:
            self.multiplier = 20

        # Make sure f_min < f_max
        if self.f_min >= self.f_max:
            err_msg = "Require f_min: %f < f_max: %f" % (
                self.f_min,
                self.f_max,
            )
            logger.error(err_msg, exc_info=True)

        # Filter definition
        if self.filter_shape == 'gammatone':
            hz = generate_center_frequencies(self.f_min, self.f_max, self.n_mels + 1)[1:].copy()
            hz = torch.tensor(hz)
            band = torch.ones(hz.shape)

            self.band = band
            self.f_central = hz
        else:
            mel = torch.linspace(to_mel(self.f_min), to_mel(self.f_max), self.n_mels + 2)
            hz = to_hz(mel)

            # Computation of the filter bands
            band = hz[1:] - hz[:-1]
            self.band = band[:-1]
            self.f_central = hz[1:-1]
    
        # Adding the central frequency and the band to the list of nn param
        if not self.freeze:
            self.f_central = torch.nn.Parameter(
                self.f_central / (self.sample_rate * self.param_change_factor)
            )
            self.band = torch.nn.Parameter(
                self.band / (self.sample_rate * self.param_change_factor)
            )

        # Frequency axis
        all_freqs = torch.linspace(0, self.sample_rate // 2, self.n_stft)

        # Replicating for all the filters
        self.all_freqs_mat = all_freqs.repeat(self.f_central.shape[0], 1)

    def forward(self, spectrogram):
        """Returns the FBANks.

        Arguments
        ---------
        x : tensor
            A batch of spectrogram tensors.
        """
        self.all_freqs_mat = self.all_freqs_mat.to(spectrogram.device)

        # clip f_central
        if self.freeze is False and self.fc_ranges is not None:
            f_central = self._f_central_constraint(self.f_central)
        else:
            f_central = self.f_central

        # Computing central frequency and bandwidth of each filter
        f_central_mat = f_central.repeat(self.all_freqs_mat.shape[1], 1).transpose(0, 1)
        band_mat = self.band.repeat(self.all_freqs_mat.shape[1], 1).transpose(0, 1)

        # Uncomment to print filter parameters
        # print(self.f_central*self.sample_rate * self.param_change_factor)
        # print(self.band*self.sample_rate* self.param_change_factor)

        # Creation of the multiplication matrix. It is used to create
        # the filters that average the computed spectrogram.
        if not self.freeze:
            f_central_mat = f_central_mat * (
                self.sample_rate
                * self.param_change_factor
                * self.param_change_factor
            )
            band_mat = band_mat * (
                self.sample_rate
                * self.param_change_factor
                * self.param_change_factor
            )

        # Regularization with random changes of filter central frequency and band
        elif self.param_rand_factor != 0 and self.training:
            rand_change = (
                1.0
                + torch.rand(2) * 2 * self.param_rand_factor
                - self.param_rand_factor
            )
            f_central_mat = f_central_mat * rand_change[0]
            band_mat = band_mat * rand_change[1]

        fbank_matrix = self._create_fbank_matrix(f_central_mat, band_mat)

        sp_shape = spectrogram.shape

        # Managing multi-channels case (batch, time, channels)
        if len(sp_shape) == 4:
            spectrogram = spectrogram.permute(0, 3, 1, 2)
            spectrogram = spectrogram.reshape(
                sp_shape[0] * sp_shape[3], sp_shape[1], sp_shape[2]
            )

        # FBANK computation
        fbanks = torch.matmul(spectrogram, fbank_matrix.to(spectrogram.device))
        if self.log_mel:
            fbanks = self._amplitude_to_DB(fbanks)

        # Reshaping in the case of multi-channel inputs
        if len(sp_shape) == 4:
            fb_shape = fbanks.shape
            fbanks = fbanks.reshape(
                sp_shape[0], sp_shape[3], fb_shape[1], fb_shape[2]
            )
            fbanks = fbanks.permute(0, 2, 3, 1)

        return fbanks

    def get_filters(self):
        f_central_mat = self.f_central.repeat(self.all_freqs_mat.shape[1], 1).transpose(0, 1)
        band_mat = self.band.repeat(self.all_freqs_mat.shape[1], 1).transpose(0, 1)

        if not self.freeze:
            f_central_mat = f_central_mat * (
                self.sample_rate
                * self.param_change_factor
                * self.param_change_factor
            )
            band_mat = band_mat * (
                self.sample_rate
                * self.param_change_factor
                * self.param_change_factor
            )

        return self._create_fbank_matrix(f_central_mat, band_mat)

    def _f_central_constraint(self, f_central):
        f_ranges = []
        for f_range, f in zip(self.fc_ranges, f_central):
            if f_range is None:
                f_ranges.append([f, f])
            else:
                f_ranges.append([f_range[0] / self.sample_rate, f_range[1] / self.sample_rate])

        f_ranges = torch.tensor(f_ranges).to(f_central.device)

        return torch.clamp(f_central, f_ranges[:, 0], f_ranges[:, 1])

    def _triangular_filters(self, all_freqs, f_central, band):
        """Returns fbank matrix using triangular filters.

        Arguments
        ---------
        all_freqs : Tensor
            Tensor gathering all the frequency points.
        f_central : Tensor
            Tensor gathering central frequencies of each filter.
        band : Tensor
            Tensor gathering the bands of each filter.
        """

        # Computing the slops of the filters
        slope = (all_freqs - f_central) / band
        left_side = slope + 1.0
        right_side = -slope + 1.0

        # Adding zeros for negative values
        zero = torch.zeros(1, device=f_central.device)
        fbank_matrix = torch.max(
            zero, torch.min(left_side, right_side)
        ).transpose(0, 1)

        return fbank_matrix

    def _rectangular_filters(self, all_freqs, f_central, band):
        """Returns fbank matrix using rectangular filters.

        Arguments
        ---------
        all_freqs : Tensor
            Tensor gathering all the frequency points.
        f_central : Tensor
            Tensor gathering central frequencies of each filter.
        band : Tensor
            Tensor gathering the bands of each filter.
        """

        # cut-off frequencies of the filters
        low_hz = f_central - band
        high_hz = f_central + band

        # Left/right parts of the filter
        left_side = right_size = all_freqs.ge(low_hz)
        right_size = all_freqs.le(high_hz)

        fbank_matrix = (left_side * right_size).float().transpose(0, 1)

        return fbank_matrix

    def _gaussian_filters(self, all_freqs, f_central, band, smooth_factor=torch.tensor(2)):
        """Returns fbank matrix using gaussian filters.

        Arguments
        ---------
        all_freqs : Tensor
            Tensor gathering all the frequency points.
        f_central : Tensor
            Tensor gathering central frequencies of each filter.
        band : Tensor
            Tensor gathering the bands of each filter.
        smooth_factor: Tensor
            Smoothing factor of the gaussian filter. It can be used to employ
            sharper or flatter filters.
        """
        fbank_matrix = torch.exp(
            -0.5 * ((all_freqs - f_central) / (band / smooth_factor)) ** 2
        ).transpose(0, 1)

        return fbank_matrix

    def _gammatone_filters(self, all_freqs, f_central, band):
        """Returns fbank matrix using gaussian filters.

        Arguments
        ---------
        f_central : Tensor
            Tensor gathering central frequencies of each filter.
        band : Tensor
            Tensor gathering the bands of each filter.
        """

        def _Dif(u, a):
            return u - a.reshape(self.n_mels, 1)
        
        # init vars
        EarQ = 9.26449
        minBW = 24.7
        order = 4

        f_central = torch.mean(f_central, dim=-1)
        band = torch.mean(band, dim=-1)

        fbank_matrix = torch.ones(self.n_mels, self.n_fft)
        T = 1 / self.sample_rate
        n = 4

        u = torch.exp(1j * 2 * np.pi * torch.arange(self.n_fft // 2 + 1) / self.n_fft)
        idx = torch.arange(self.n_fft // 2 + 1)

        ERB = band * ((f_central / EarQ) ** order + minBW**order) ** (1 / order)
        ERB = ERB.to(self.device_inp)
        B = 1.019 * 2 * torch.pi * ERB

        # compute input vars
        wT = 2 * f_central * torch.pi * T
        wT = wT.to(self.device_inp)

        pole = torch.exp(1j * wT) / torch.exp(B * T)

        # compute gain and A matrix
        A, Gain = compute_gain(f_central.detach().cpu().numpy(), B.detach().cpu().numpy(), wT.detach().cpu().numpy(), T)
        A = torch.tensor(A)
        Gain = torch.tensor(Gain)

        # compute fbank
        fbank_matrix[:, idx] = (
            (T**4 / Gain.reshape(self.n_mels, 1))
            * torch.abs(_Dif(u, A[0]) * _Dif(u, A[1]) * _Dif(u, A[2]) * _Dif(u, A[3]))
            * torch.abs(_Dif(u, pole) * _Dif(u, pole.conj())) ** (-n)
        ).float()

        fbank_matrix = torch.stack([f / torch.max(f) for f in fbank_matrix[:, idx]])

        return fbank_matrix.T

    def _create_fbank_matrix(self, f_central_mat, band_mat):
        """Returns fbank matrix to use for averaging the spectrum with
           the set of filter-banks.

        Arguments
        ---------
        f_central : Tensor
            Tensor gathering central frequencies of each filter.
        band : Tensor
            Tensor gathering the bands of each filter.
        smooth_factor: Tensor
            Smoothing factor of the gaussian filter. It can be used to employ
            sharper or flatter filters.
        """
        if self.filter_shape == "triangular":
            fbank_matrix = self._triangular_filters(self.all_freqs_mat, f_central_mat, band_mat)

        elif self.filter_shape == "rectangular":
            fbank_matrix = self._rectangular_filters(self.all_freqs_mat, f_central_mat, band_mat)

        elif self.filter_shape == "gaussian":
            fbank_matrix = self._gaussian_filters(self.all_freqs_mat, f_central_mat, band_mat, smooth_factor=self.smooth_factor)

        elif self.filter_shape == "gammatone":
            fbank_matrix = self._gammatone_filters(self.all_freqs_mat, f_central_mat, band_mat)
        
        else:
            raise Exception(f'Unsupport filter shape: {self.filter_shape}')

        # normalize each filter into [0, 1]
        idx = torch.arange(fbank_matrix.shape[1])
        fbank_matrix = torch.stack([fbank_matrix[:, id] / torch.max(fbank_matrix[:, id]) for id in idx], dim=-1)

        return fbank_matrix

    def _amplitude_to_DB(self, x):
        """Converts  linear-FBANKs to log-FBANKs.

        Arguments
        ---------
        x : Tensor
            A batch of linear FBANK tensors.

        """

        x_db = self.multiplier * torch.log10(torch.clamp(x, min=self.amin))
        x_db -= self.multiplier * self.db_multiplier

        # Setting up dB max. It is the max over time and frequency,
        # Hence, of a whole sequence (sequence-dependent)
        new_x_db_max = x_db.amax(dim=(-2, -1)) - self.top_db

        # Clipping to dB max. The view is necessary as only a scalar is obtained
        # per sequence.
        x_db = torch.max(x_db, new_x_db_max.view(x_db.shape[0], 1, 1))

        return x_db

#############################


def to_mel(hz):
    return 2595 * math.log10(1 + hz / 700)


def to_hz(mel):
    return 700 * (10 ** (mel / 2595) - 1)