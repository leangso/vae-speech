import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from speechbrain.nnet.CNN import GaborConv1d, SincConv, get_padding_elem
from speechbrain.nnet.normalization import PCEN


class FilterConv(nn.Module):
    
    def __init__(
        self,
        in_channels=1,
        out_channels=40,
        kernel_size=400,
        stride=160,
        padding='same',
        padding_mode='reflect',
        sample_rate=16000,
        min_freq=50
    ):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.stride = stride
        self.dilation = 1
        self.padding = padding
        self.padding_mode = padding_mode
        self.sample_rate = sample_rate
        self.min_freq = min_freq
    
        self._init_filters()

    def forward(self, x):
        """Returns the output of the convolution.

        Args:
            x : batch, time
        
        Outputs:
            y : batch, out_channel, time

        """
        x = x.transpose(1, -1)

        unsqueeze = x.ndim == 2
        if unsqueeze:
            x = x.unsqueeze(1)

        if self.padding == "same":
            x = self.manage_padding(x, self.kernel_size, self.dilation, self.stride)

        elif self.padding == "causal":
            num_pad = (self.kernel_size - 1) * self.dilation
            x = F.pad(x, (num_pad, 0))

        elif self.padding == "valid":
            pass

        else:
            raise ValueError(
                "Padding must be 'same', 'valid' or 'causal'. Got %s."
                % (self.padding)
            )

        filters = self._get_filters(x.device)
        filters = filters.unsqueeze(1).float()

        wx = F.conv1d(
            x,
            filters,
            stride=self.stride,
            padding=0,
            dilation=self.dilation,
            groups=self.in_channels,
        )

        if unsqueeze:
            wx = wx.squeeze(1)

        return wx

    def _get_filters(self, device='cpu'):        
        raise Exception('Unimplemented')

    def _init_filters(self):
        raise Exception('Unimplemented')

    def _manage_padding(self, x, kernel_size: int, dilation: int, stride: int):
        """This function performs zero-padding on the time axis
        such that their lengths is unchanged after the convolution.

        Arguments
        ---------
        x : torch.Tensor
            Input tensor.
        kernel_size : int
            Size of kernel.
        dilation : int
            Dilation used.
        stride : int
            Stride.
        """

        # Detecting input shape
        L_in = self.in_channels

        # Time padding
        padding = get_padding_elem(L_in, stride, kernel_size, dilation)

        # Applying padding
        x = F.pad(x, padding, mode=self.padding_mode)

        return x

#####################################

class GaborConv(nn.Module):

    def __init__(
            self, 
            in_channels=1,
            out_channels=40,
            kernel_size=401,
            stride=160,
            padding='valid',
            bias=False,
            n_fft=512,
            sample_rate=16000,
            min_freq=50.0,
            use_pcen=False,
            learnable_pcen=False):
        
        super(GaborConv, self).__init__()

        # complex convolution
        self.conv = GaborConv1d(
            in_channels=in_channels,
            out_channels=2 * out_channels, # one for real and another for imagine filter
            kernel_size=kernel_size, 
            stride=stride,
            padding=padding,
            bias=bias,
            n_fft=n_fft,
            sample_rate=sample_rate,
            min_freq=min_freq,
            skip_transpose=True,
            sort_filters=True)

        if use_pcen:
            self.pcen = PCEN(
                out_channels,
                alpha=0.96,
                smooth_coef=0.04,
                delta=2.0,
                floor=1e-12,
                trainable=learnable_pcen,
                per_channel_smooth_coef=True,
                skip_transpose=True,
            )

        self.use_pcen = use_pcen
        self.out_channels = out_channels

    def forward(self, x):
        z = self.conv(x)
        # compute magnitude from real and imaginary filters
        z = torch.complex(z[:, :self.out_channels, :], z[:, self.out_channels:, :])
        z = torch.abs(z)

        if self.use_pcen:
            z = self.pcen(z)

        return z

#####################################

class GammatoneConv(FilterConv):
    """"
        1d convolution using gammatone filters. 
        The implementation is based on "Learnable filter-banks for CNN-based audio applications".
        Codes: https://github.com/epfl-lts2/learnable-filterbanks
    """

    def __init__(
        self,
        in_channels=1,
        out_channels=40,
        kernel_size=401,
        stride=160,
        padding='same',
        padding_mode='reflect',
        sample_rate=16000,
        order=4,
        min_freq=50
    ):
        self.order = order

        super(GammatoneConv, self).__init__(
            in_channels,
            out_channels,
            kernel_size,
            stride,
            padding,
            padding_mode,
            sample_rate,
            min_freq
        )

    def _get_filters(self, device='cpu'):
        fc = torch.abs(self.fc_.view(-1, 1))
        bw = torch.abs(self.bw_.view(-1, 1))

        min_freq = self.min_freq / self.sample_rate
        high_freq = (self.sample_rate / 2 - self.min_freq) / self.sample_rate
        fc = torch.clamp(
            fc,
            min_freq,
            high_freq,
        )

        n = self.n_.to(device)

        alpha = self.order - 1
        gtone = n**(alpha) * torch.exp(-2 * np.pi * bw * n) * torch.cos(2 * np.pi * fc * n)
        gnorm = (4 * np.pi * bw)**((2 * alpha + 1) / 2) / torch.sqrt(torch.exp(torch.lgamma(torch.tensor(2 * alpha + 1)))) * np.sqrt(2)

        filters = gtone * gnorm

        return filters

    def _init_filters(self):
        fc = torch.linspace(self.min_freq, self.sample_rate / 2, self.out_channels + 2)[1:-1] / self.sample_rate
        bw = torch.ones(self.out_channels) * 1 / self.out_channels / 4

        self.fc_ = nn.Parameter(fc)
        self.bw_ = nn.Parameter(bw)

        # generate a time vector 'n'
        self.n_ = torch.arange(0, self.kernel_size - 1).view(1, -1)

#####################################

class GaussianConv(FilterConv):
    """"
        1d convolution using guassian filters. 
        The implementation is based on "Learnable filter-banks for CNN-based audio applications".
        Codes: https://github.com/epfl-lts2/learnable-filterbanks
    """

    def __init__(
        self,
        in_channels=1,
        out_channels=40,
        kernel_size=401,
        stride=160,
        padding='same',
        padding_mode='reflect',
        sample_rate=16000,
        min_freq=50
    ):
        super(GaussianConv, self).__init__(
            in_channels,
            out_channels,
            kernel_size,
            stride,
            padding,
            padding_mode,
            sample_rate,
            min_freq
        )

    def _get_filters(self, device='cpu'):
        fc = torch.abs(self.fc_.view(-1, 1))
        sigma = torch.abs(self.sigma_.view(-1, 1))

        min_freq = self.min_freq / self.sample_rate
        high_freq = (self.sample_rate / 2 - self.min_freq) / self.sample_rate
        fc = torch.clamp(
            fc,
            min_freq,
            high_freq,
        )

        n = self.n_.to(device)

        # generate a cosine window
        cos_win = torch.cos(fc * 2 * np.pi * n)

        # generate a Gaussian window
        gaus_win = torch.sqrt(2 / (np.sqrt(np.pi) * sigma))
        gaus_win = gaus_win * torch.exp(-n**2 / (2 * sigma**2))

        # combine the cosine and Gaussian windows to form the Gammatone filter kernel
        filters = gaus_win * cos_win

        return filters

    def _init_filters(self):
        fc = torch.linspace(self.min_freq, self.sample_rate / 2, self.out_channels + 2)[1:-1] / self.sample_rate
        sigma = torch.ones(fc.shape) * self.out_channels

        self.fc_ = nn.Parameter(fc)
        self.sigma_ = nn.Parameter(sigma)

        # generate a time vector 'n' centered at 0
        self.n_ = torch.arange(0, self.kernel_size) - (self.kernel_size - 1.0) / 2.0
