import argparse
import os
import shutil
from os import path

from data.datasource import SamplingSpeechDataSource
from experiment.experiment import load_exp_configs
from experiment.vq_vae import VQVAE_EXP
from util.logger import init_root_logger
from lightning.pytorch import seed_everything

seed_everything(0, workers=False)


if __name__ == "__main__":
    root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    default_exp_conf_file = path.join(root_dir, 'conf', 'exp.yaml')

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--config_file', nargs='?', default=default_exp_conf_file, type=str, help='The experiment configuration file')
    parser.add_argument('--clean', type=bool, default=False)
    parser.add_argument('--train', type=bool, default=True)
    parser.add_argument('--test', type=bool, default=False)

    args = parser.parse_args()

    if not path.exists(args.config_file):
        print(f'Experiement configuration file not found: {args.config_file}')
        exit(1)

    # load configurations
    print(f'Loading experiment configuration: {args.config_file}')
    configs = load_exp_configs(args.config_file)

    config = configs[0]
    output_dir = config['output_dir']
    if args.clean is True:
        shutil.rmtree(output_dir, ignore_errors=True)

    os.makedirs(output_dir, exist_ok=True)

    # init main logger
    write_log = config.get('write_log')
    write_log_fname = config.get('write_log_fname')
    logger = init_root_logger(output_dir=output_dir, write_file=write_log, fname=write_log_fname)

    # init data source
    logger.debug('>>> Init data source')
    data_source = SamplingSpeechDataSource.from_config(config)

    for idx, conf in enumerate(configs):
        logger.debug(f'>>> Init experiment: {conf["name"]}')

        exp = VQVAE_EXP(**conf)

        if exp.skip is True:
            logger.debug(f'>>> Skip experiment: {exp.name}')
            continue

        logger.debug(f'>>> Start experiment: {exp.name}')
    
        # train
        if args.train is True:    
            exp.fit(data_source=data_source)   

        # test
        if args.test is True:
            exp.test(data_source=data_source)

        logger.debug(f'<<< End experiment: {exp.name}')

        exp.shutdown()
