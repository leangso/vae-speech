import logging
import os, glob
import sys
import warnings
from logging import handlers

warnings.filterwarnings("ignore", ".*") # disable all warning, mostly pytorch lightning

root = 'main'
format = logging.Formatter("%(asctime)s | %(name)-20.20s:%(lineno)03d | %(levelname)-5s : %(message)s", datefmt='%Y-%m-%d %H:%M:%S')


class TruncateNameFilter(logging.Filter):

    def __init__(self, name='', max_length=20):
        super(TruncateNameFilter, self).__init__(name)
        self.max_length = max_length

    def filter(self, record):
        if len(record.name) > self.max_length:
            record.name = '...' + record.name[(-self.max_length + 3):]

        return True    


class StreamToLogger(object):
   
    def __init__(self, loggers=[], level=logging.DEBUG):
       self.loggers = loggers
       self.level = level
       self.linebuf = ''
       self.encoding = 'utf-8'

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            line = line.rstrip()
            if line != '':
                for logger in self.loggers:
                    logger.log(self.level, line)

    def flush(self):
        pass

    def remove(self, logger):
        self.loggers.remove(logger)

    def append(self, logger):
        self.loggers.append(logger)


def get_logger(name=None):
    return logging.getLogger(root) if name is None else logging.getLogger(root + '.' + name)


def init_root_logger(level=logging.DEBUG, output_dir='./', write_file=False, fname=None):
    logger = logging.getLogger(root)
    logger.setLevel(level)

    # console handler
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(format)
    console_handler.addFilter(TruncateNameFilter(max_length=20))
    logger.addHandler(console_handler)

    # ligthning
    pl_logger = logging.getLogger("lightning.pytorch")
    pl_logger.removeHandler(pl_logger.handlers[0]) # remove default lightning handler
    pl_logger.setLevel(logging.INFO)
    pl_logger.addHandler(console_handler)

    if write_file is True:
        fname = fname if fname is not None else 'log'
        log_file = os.path.join(output_dir, fname + '.txt')
        
        file_handler = handlers.RotatingFileHandler(log_file, maxBytes=10485760, backupCount=7)
        file_handler.setFormatter(format)
        file_handler.addFilter(TruncateNameFilter(max_length=20))    

        logger.addHandler(file_handler)
        pl_logger.addHandler(file_handler)

    # redirect stdout to logger
    sys.stdout = StreamToLogger([logger], level)

    return logger


def attach_logger(logger):
    root_logger = get_logger()
    for handler in logger.handlers:
        root_logger.addHandler(handler)

def detach_logger(logger):
    root_logger = get_logger()
    for handler in logger.handlers:
        root_logger.removeHandler(handler)

