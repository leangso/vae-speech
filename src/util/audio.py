import random
from math import ceil

import librosa
import numpy as np

def load_audio(file: str, sr: int = 16000, mono: bool = True, duration: float = None, normalize: bool = True) -> np.ndarray:
    signal, sr = librosa.load(file, sr=sr, mono=mono, duration=duration)
    if normalize is True:
        signal /= np.abs(signal).max()
    return signal, sr


def trim_signal(signal: np.ndarray, length: int, random_trim: bool = False):
    if len(signal) <= length:
        pad = length - len(signal)
        signal = np.concatenate((signal, np.zeros(pad)))
    else:
        start = random.randint(0, len(signal) - length  - 1) if random_trim is True else 0
        signal = signal[start : start + length]
    
    return signal


def chunk_signal(signal: np.ndarray, chunk_len: int = None, padding: bool = True):
    num_chunk = ceil(len(signal) / float(chunk_len))
    chunks = [signal[i * chunk_len:(i + 1) * chunk_len] for i in range(int(num_chunk))]
    if padding is True:
        chunks[-1] = trim_signal(chunks[-1], length=chunk_len)
    
    return chunks
