import os
import json
import yaml
import torch
import util


def scan_dir(path) -> list:
    dirs = list(os.scandir(path))
    dirs.sort(key=lambda x: x.name)
    return dirs


def read_json_file(file: str, **kwargs) -> dict:
    with open(file, 'r', encoding='utf-8') as file:
        return json.load(file, **kwargs)


def save_json_file(file: str, data: dict) -> None:
    with open(file, 'w', encoding='utf-8') as outfile:
        json.dump(data, outfile)


def read_yaml_file(file: str) -> dict:
    with open(file, 'r', encoding='utf-8') as file:
        return yaml.safe_load(file)


def save_yaml_file(data: dict, file:str, **kwargs) -> None:
    with open(file, 'w') as file:
        yaml.safe_dump(data, file, **kwargs)


def load_model(cls, ckpt_file, hyps_file=None, **kwargs):
    ckpt = torch.load(ckpt_file)

    state_dict = {}
    for key, value in ckpt['state_dict'].items():
        new_key = key.replace('model.', '', 1)
        state_dict[new_key] = value

    hyps = ckpt.get('hyper_parameters')
    if hyps is None:
        hyps = {}
    
    if hyps_file is not None:
        file_hyps = read_yaml_file(hyps_file)
        util.merge_dict(hyps, file_hyps)
    
    util.merge_dict(hyps, kwargs)
    
    model = cls(**hyps)
    model.load_state_dict(state_dict)

    return model
