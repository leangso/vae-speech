import numpy as np
import inspect
import torch


def get(config, key, default=None):
    value = config.get(key)
    return default if value is None else value


def merge_dict(dct: dict, merge_dct: dict):
    for k, v in iter(merge_dct.items()):
        if k in dct and isinstance(dct[k], dict) and isinstance(merge_dct[k], dict):
            merge_dict(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]


def get_method_param_names(method):
    params = inspect.getfullargspec(method)[0]
    params = [ param for param in params if param != 'self']
    return params


def split_ts_complex(x: torch.Tensor):
    num_dim = len(x.shape)
    return torch.cat((x.real.unsqueeze(num_dim), x.imag.unsqueeze(num_dim)), dim=num_dim)


def merge_ts_complex(x: torch.Tensor):
    return torch.complex(x[..., 0], x[..., 1])


# wrapper to support torch/numpy arrays

def sign(x):
    isnumpy = isinstance(x, np.ndarray)
    isscalar = np.isscalar(x)
    return np.sign(x) if isnumpy or isscalar else x.sign()


def log1p(x):
    isnumpy = isinstance(x, np.ndarray)
    isscalar = np.isscalar(x)
    return np.log1p(x) if isnumpy or isscalar else x.log1p()


def abs(x):
    isnumpy = isinstance(x, np.ndarray)
    isscalar = np.isscalar(x)
    return np.abs(x) if isnumpy or isscalar else x.abs()


def asint(x):
    isnumpy = isinstance(x, np.ndarray)
    isscalar = np.isscalar(x)
    return x.astype(int) if isnumpy else int(x) if isscalar else x.long()


def asfloat(x):
    isnumpy = isinstance(x, np.ndarray)
    isscalar = np.isscalar(x)
    return x.astype(np.float32) if isnumpy else float(x) if isscalar else x.float()


# mulaw

def mulaw(x, mu=256):
    return sign(x) * log1p(mu * abs(x)) / log1p(mu)


def inv_mulaw(y, mu=256):
    return sign(y) * (1.0 / mu) * ((1.0 + mu) ** abs(y) - 1.0)


def mulaw_encode(x, mu=256):
    """"
        x: must be in [-1, 1]
    """
    mu = mu - 1
    y = mulaw(x, mu)
    return asint((y + 1) / 2 * mu) # [-1, 1] -> [0, mu]


def mulaw_decode(y, mu=256):
    mu = mu - 1
    y = 2 * asfloat(y) / mu - 1 # [0, mu] -> [-1, 1]
    return inv_mulaw(y, mu)
