# python 3.8.18
lightning==2.1.3
tensorboard==2.14.0
six==1.16.0
librosa==0.10.1
spafe==0.3.2
kaldiio==2.18.0
pydub==0.25.1
speechbrain==0.5.16
vector_quantize_pytorch==1.12.17